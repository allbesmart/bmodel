import json
import sqlite3 as lite
import sys
import datetime

con1 = None
con2 = None
#change this for other testbeds
observationsDatabaseName='energy-iot.sqlite3'
digestDatabaseName='smartics.sqlite3'
testbed_latitude=51.2433445

try:
    numberArguments=len(sys.argv)
    #print 'Number of arguments:', numberArguments, 'arguments.'
    #print 'Argument List:', str(sys.argv)    
    
    if numberArguments>=3: #update results for the givem period
        argDatetimeBeginStr=sys.argv[1]
        argDatetimeEndStr=sys.argv[2]
        dateTimeBegin = datetime.datetime.strptime(argDatetimeBeginStr,"%Y-%m-%dT%H:%M:%SZ")
        dateTimeEnd = datetime.datetime.strptime(argDatetimeEndStr,"%Y-%m-%dT%H:%M:%SZ")
    else: #update results for the last week
        dateTimeEnd = datetime.datetime.now()
        dateTimeEnd = dateTimeEnd + datetime.timedelta(hours=1)  #try the next hour (if the cron runs at 55 minutes, for example...
        dateTimeEnd=dateTimeEnd.replace(minute=0,second=0,microsecond=0)
        dateTimeBegin = dateTimeEnd - datetime.timedelta(days=7)
        #print(dateTimeBegin)
        #print(dateTimeEnd)

    con1=lite.connect(observationsDatabaseName)
    cur1=con1.cursor()

    con2=lite.connect(digestDatabaseName)
    cur2=con2.cursor()
    

    #print dateTimeBegin
    #print dateTimeEnd
    
    dateTimeBegin=dateTimeBegin.replace(minute=0,second=0)
    dateTimeEnd=dateTimeEnd.replace(minute=0,second=0)
    
    print 'Generating data from ' + str(dateTimeBegin) + ' to ' + str(dateTimeEnd)
    
    dateTime=dateTimeBegin
    
    while dateTime<dateTimeEnd:
        periodBegin=dateTime.strftime("%Y-%m-%dT%H:%M:%SZ")
        
        #avancar uma hora, para calcular as medias da ultima hora
        dateTime = dateTime + datetime.timedelta(minutes=60)
        periodEnd=dateTime.strftime("%Y-%m-%dT%H:%M:%SZ")
        
        #se este periodo de uma hora ultrapassa a datahora final
        if dateTime>dateTimeEnd:
            break
        
        print 'Writing data for ' + periodEnd
        
        #now, calculate total power
        cur1.execute("SELECT SUM(average_power) FROM (select AVG(value) AS average_power FROM observation WHERE lat=? AND datetime>? AND datetime<? AND qk='Power' GROUP BY id_sensor);",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            totalPower=queryResult[0]
            #print("Total Power: " + str(totalPower))
        else:
            totalPower=None

        #now, calculate average internal humidity
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='Humidity';",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            relativeHumidity=queryResult[0]
            #print("Relative Humidity: " + str(relativeHumidity))
        else:
            relativeHumidity= None

        #now, get the internal temperature
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND id_sensor=315;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            interiorTemperature=queryResult[0]
            #print("interior Temperature: " + str(interiorTemperature))
        else:
            interiorTemperature= None

        #now, compute the human presence
        cur1.execute("SELECT id_sensor,value from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='Distance' ORDER BY id_sensor;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchall()
        if (queryResult != None and len(queryResult)>0):
            lastdistanceSensorID = None
            lastdistanceSensorValue = None
            numberMovements=0;
            for sensor in queryResult:
                distanceSensorID=sensor[0]
                distanceSensorValue=sensor[1]
                #print ("Sensor: " + str(distanceSensorID) + " , value: " + str(distanceSensorValue))
                if (lastdistanceSensorID == distanceSensorID and lastdistanceSensorValue!= None and distanceSensorValue != None):
                    #if the distance changed by more than 10% that means a movement has ocurred
                    if (((abs(lastdistanceSensorValue-distanceSensorValue)) / min(distanceSensorValue,lastdistanceSensorValue)) > 0.2):
                        numberMovements = numberMovements + 1 ;
                
                lastdistanceSensorID=distanceSensorID;
                lastdistanceSensorValue=distanceSensorValue;
            #print(str(numberMovements) + " movements detected")
        else:
            numberMovements= None
        
        #now, write it in the database
        cur2.execute("INSERT OR REPLACE INTO observation(datetime,total_power,humidity,internal_temperature,human_activity) VALUES (?,?,?,?,?);",(periodEnd,totalPower,relativeHumidity,interiorTemperature,numberMovements,))
        con2.commit()
            
except lite.Error, e:
    print ('Error %s:' % e.args[0])
    sys.exit(1)

finally:
    if con1:
        con1.close()

    if con2:
        con2.close()



