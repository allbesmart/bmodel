import sqlite3 as lite
import sys
import datetime
import numpy as np

con = None
#change this for other testbeds
databaseName='/home/eiot/realdc.sqlite3'

#=============================================================================================================
#this function reads observations from a specific date
#obsDatetime: a datetime object with the date and time of the required observations
def getObservations(obsDatetime):
    try:
        con=lite.connect(databaseName)
        cur=con.cursor()

        #truncate it to fixed hour
        obsDatetime=obsDatetime.replace(minute=0,second=0,microsecond=0)        
        #print("Getting observations from ")
        #print(obsDatetime)
        
        #convert it to database datetime format
        dbDatetime=obsDatetime.strftime("%Y-%m-%dT%H:%M:00Z")
        #print(dbDatetime)
        
        #now, get the observations from the database
        cur.execute("SELECT datetime,total_power,humidity,internal_temperature,external_temperature,rainfall,water_temperature1,water_temperature2,wind_chill,wind_speed FROM observation WHERE datetime=?;",(dbDatetime,))
        queryResult = cur.fetchone()
        if (queryResult != None and queryResult[0] != None):
            #print(queryResult)
            return {    'datetime': queryResult[0],
                        'total_power': queryResult[1],
                        'humidity': queryResult[2],
                        'internal_temperature': queryResult[3],
                        'external_temperature': queryResult[4],
                        'rainfall': queryResult[5],
                        'water_temperature1': queryResult[6],
                        'water_temperature2': queryResult[7],
                        'wind_chill': queryResult[8],
                        'wind_speed': queryResult[9]
                    }
            
        else:
            return None
            
        con.close()
        
    
    #except lite.Error, e:
    except lite.Error as e:
        print ('Error %s:' % e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()


            
#=============================================================================================================
#this function reads observations from a specific period
#obsDatetime1: a datetime object with the initial date and time of the required period
#obsDatetime2: a datetime object with the final date and time of the required period
def getBulkObservationsOld(obsDatetime1,obsDatetime2):
    try:
        con=lite.connect(databaseName)
        cur=con.cursor()

        #truncate it to fixed hour
        obsDatetime1=obsDatetime1.replace(minute=0,second=0,microsecond=0)        
        obsDatetime2=obsDatetime2.replace(minute=0,second=0,microsecond=0)        
        #print("Getting observations from ")
        #print(obsDatetime)
        
        #convert it to database datetime format
        dbDatetime1=obsDatetime1.strftime("%Y-%m-%dT%H:%M:00Z")
        dbDatetime2=obsDatetime2.strftime("%Y-%m-%dT%H:%M:00Z")
        #print(dbDatetime)
        
        returnResult={};
        #now, get the observations from the database
        cur.execute("SELECT datetime,total_power,humidity,internal_temperature,external_temperature,rainfall,water_temperature1,water_temperature2,wind_chill,wind_speed FROM observation WHERE datetime>=? AND datetime<=? ORDER BY datetime;",(dbDatetime1,dbDatetime2,))
        queryResult = cur.fetchall()
        for row in queryResult:
            if (row != None and row[0] != None):
                tempResults = {  'total_power': row[1],
                            'humidity': row[2],
                            'internal_temperature': row[3],
                            'external_temperature': row[4],
                            'rainfall': row[5],
                            'water_temperature1': row[6],
                            'water_temperature2': row[7],
                            'wind_chill': row[8],
                            'wind_speed': row[9]
                }
                #print tempResults
                returnResult.update({ row[0] : tempResults})
        
        return returnResult;
            
        con.close()
        
    
    #except lite.Error, e:
    except lite.Error as e:
        print ('Error %s:' % e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()


            

#=============================================================================================================
#this function reads observations from a specific period, fixing void date points with interpolated data
#obsDatetime1: a datetime object with the initial date and time of the required period
#obsDatetime2: a datetime object with the final date and time of the required period
def getBulkObservations(obsDatetime1,obsDatetime2):
    try:
        con=lite.connect(databaseName)
        cur=con.cursor()

        #truncate it to fixed hour
        obsDatetime1=obsDatetime1.replace(minute=0,second=0,microsecond=0)        
        obsDatetime2=obsDatetime2.replace(minute=0,second=0,microsecond=0)        
        #print("Getting observations from ")
        #print(obsDatetime)
        
        #convert it to database datetime format
        dbDatetime1=obsDatetime1.strftime("%Y-%m-%dT%H:%M:00Z")
        dbDatetime2=obsDatetime2.strftime("%Y-%m-%dT%H:%M:00Z")
        #print(dbDatetime1)
        #print(dbDatetime2)
        
        returnResult={};
        
        x1=[]
        x2=[]
        x3=[]
        x4=[]
        x5=[]
        x6=[]
        x7=[]
        x8=[]
        x9=[]
        
        y1=[]
        y2=[]
        y3=[]
        y4=[]
        y5=[]
        y6=[]
        y7=[]
        y8=[]
        y9=[]
        
        #now, get the observations from the database
        cur.execute("SELECT datetime,total_power,humidity,internal_temperature,external_temperature,rainfall,water_temperature1,water_temperature2,wind_chill,wind_speed FROM observation WHERE datetime>=? AND datetime<=? ORDER BY datetime;",(dbDatetime1,dbDatetime2,))
        queryResult = cur.fetchall()
        for row in queryResult:
            if (row != None and row[0] != None):
                dbDatetime=row[0]
                pythonDatetime = datetime.datetime.strptime(dbDatetime,'%Y-%m-%dT%H:%M:%SZ')
                #print pythonDatetime
                diff = pythonDatetime - obsDatetime1
                hourIndex= (diff.total_seconds()) / 3600
                #print hourIndex

                if (row[1] != None):
                    x1.append(hourIndex)
                    y1.append(row[1]) #total_power

                if (row[2] != None):
                    x2.append(hourIndex)
                    y2.append(row[2]) #humidity

                if (row[3] != None):
                    x3.append(hourIndex)
                    y3.append(row[3]) #internal_temperature

                if (row[4] != None):
                    x4.append(hourIndex)
                    y4.append(row[4]) #external_temperature
            
                if (row[5] != None):
                    x5.append(hourIndex)
                    y5.append(row[5]) #rainfall

                if (row[6] != None):
                    x6.append(hourIndex)
                    y6.append(row[6]) #water_temperature1

                if (row[7] != None):
                    x7.append(hourIndex)
                    y7.append(row[7]) #water_temperature2

                if (row[8] != None):
                    x8.append(hourIndex)
                    y8.append(row[8]) #wind_chill

                if (row[9] != None):
                    x9.append(hourIndex)
                    y9.append(row[9]) #wind_speed
                    
        con.close()
        
        if (len(y1)==0 or len(y2)==0 or len(y3)==0 or len(y4)==0): #if some variables have no data at all
            return {}

        if (len(y5)==0 or len(y6)==0 or len(y7)==0 or len(y8)==0 or len(y9)==0): #if some variables have no data at all
            return {}
        
        
        datePointer=obsDatetime1.replace(minute=0,second=0,microsecond=0)
        while (datePointer <= obsDatetime2):
            diff = datePointer - obsDatetime1
            hourIndex= (diff.total_seconds()) / 3600
            #print hourIndex
            dbDatetime=datePointer.strftime("%Y-%m-%dT%H:%M:00Z")
 
            tempResults = {  'total_power': np.interp(hourIndex, x1, y1),
                            'humidity': np.interp(hourIndex, x2, y2),
                            'internal_temperature': np.interp(hourIndex, x3, y3),
                            'external_temperature': np.interp(hourIndex, x4, y4),
                            'rainfall': np.interp(hourIndex, x5, y5),
                            'water_temperature1': np.interp(hourIndex, x6, y6),
                            'water_temperature2': np.interp(hourIndex, x7, y7),
                            'wind_chill': np.interp(hourIndex, x8, y8),
                            'wind_speed': np.interp(hourIndex, x9, y9)
            }
            #print(dbDatetime,tempResults)
            returnResult.update({ dbDatetime : tempResults})
            
            datePointer = datePointer + datetime.timedelta(hours=1)
            
        
        return returnResult;

        
    #except lite.Error, e:
    except lite.Error as e:
        print ('Error %s:' % e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()


            
            
            

            
#=============================================================================================================
#this function writes predictions for a specific date
#datetime: a datetime object with the date and time which will be be time base for predictions
#offset: an integer that represents the offset from the datetime parameter, in hours, which the prediction applies to
#preditionMethodId: an integer that identifies the prediction method used for example 1 for ARIMAX , 2 for Neural networks, 3 for...
#predictedValue: a float or double that holds the predicted value
def setPrediction(datetime,offset,preditionMethodId,predictedValue):
    try:
        con=lite.connect(databaseName)
        cur=con.cursor()

        #convert datetime object to database datetime format
        dbDatetime=datetime.strftime("%Y-%m-%dT%H:%M:00Z")
        #print(dbDatetime)
        
        #delete existing records for the same datetime,offset and prediction method
        cur.execute("DELETE FROM prediction WHERE datetime=? AND hour_offset=? AND predition_method_id=?;",(dbDatetime,offset,preditionMethodId))
        #now, write the data to the database
        cur.execute("INSERT INTO prediction(datetime,hour_offset,predition_method_id,predicted_value) VALUES (?,?,?,?);",(dbDatetime,offset,preditionMethodId,predictedValue))

        con.commit()
        con.close()
        
    
    #except lite.Error, e:
    except lite.Error as e:
        print ('Error %s:' % e.args[0])
        sys.exit(1)

    finally:
        if con:
            con.close()


            
            
            