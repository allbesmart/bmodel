<?php
$datetimeBegin=htmlspecialchars($_GET["dateBegin"]);
$datetimeEnd=htmlspecialchars($_GET["dateEnd"]);

$db = new SQLite3('/home/eiot/energy-iot.sqlite3',SQLITE3_OPEN_READONLY);

//motion sensor numbers, from 116 to 211

$sensorPosition= array(
  //sensor number, x position, y position, value
  
  //second floor
  '116' => array ('x' => 63, 'y' => 18, 'value' => 0),
  '117' => array ('x' => 94, 'y' => 18, 'value' => 0),
  '118' => array ('x' => 104, 'y' => 18, 'value' => 0),
  '119' => array ('x' => 127, 'y' => 18, 'value' => 0),

  '120' => array ('x' => 151, 'y' => 18, 'value' => 0),
  '121' => array ('x' => 180, 'y' => 18, 'value' => 0),
  '122' => array ('x' => 201, 'y' => 16, 'value' => 0),
  '123' => array ('x' => 155, 'y' => 58, 'value' => 0),
  '124' => array ('x' => 128, 'y' => 57, 'value' => 0),
  '125' => array ('x' => 97, 'y' => 57, 'value' => 0),
  '126' => array ('x' => 68, 'y' => 57, 'value' => 0),
  '127' => array ('x' => 39, 'y' => 58, 'value' => 0),
  '128' => array ('x' => 15, 'y' => 57, 'value' => 0),
  '129' => array ('x' => 204, 'y' => 69, 'value' => 0),

  '130' => array ('x' => 205, 'y' => 177, 'value' => 0),
  '131' => array ('x' => 205, 'y' => 209, 'value' => 0),
  '132' => array ('x' => 181, 'y' => 208, 'value' => 0),
  '133' => array ('x' => 156, 'y' => 166, 'value' => 0),
  '134' => array ('x' => 126, 'y' => 166, 'value' => 0),
  '135' => array ('x' => 98, 'y' => 167, 'value' => 0),
  '136' => array ('x' => 68, 'y' => 168, 'value' => 0),
  '137' => array ('x' => 25, 'y' => 173, 'value' => 0),
  '138' => array ('x' => 12, 'y' => 199, 'value' => 0),
  '139' => array ('x' => 35, 'y' => 199, 'value' => 0),

  '140' => array ('x' => 66, 'y' => 205, 'value' => 0),
  '141' => array ('x' => 90, 'y' => 206, 'value' => 0),
  '142' => array ('x' => 110, 'y' => 206, 'value' => 0),
  '143' => array ('x' => 135, 'y' => 208, 'value' => 0),
  '144' => array ('x' => 155, 'y' => 207, 'value' => 0),
  '145' => array ('x' => 208, 'y' => 148, 'value' => 0),
  '146' => array ('x' => 208, 'y' => 108, 'value' => 0),
  '147' => array ('x' => 75, 'y' => 66, 'value' => 0),
  '148' => array ('x' => 105, 'y' => 68, 'value' => 0),
  '149' => array ('x' => 131, 'y' => 68, 'value' => 0),

  '150' => array ('x' => 161, 'y' => 71, 'value' => 0),
  '151' => array ('x' => 74, 'y' => 156, 'value' => 0),
  '152' => array ('x' => 104, 'y' => 155, 'value' => 0),
  '153' => array ('x' => 133, 'y' => 155, 'value' => 0),
  '154' => array ('x' => 161, 'y' => 155, 'value' => 0),
  
  //first floor --------------------------------------------------
  '155' => array ('x' => 364, 'y' => 18, 'value' => 0),
  '156' => array ('x' => 388, 'y' => 19, 'value' => 0),
  '157' => array ('x' => 412, 'y' => 18, 'value' => 0),
  '158' => array ('x' => 438, 'y' => 17, 'value' => 0),
  '159' => array ('x' => 456, 'y' => 13, 'value' => 0),

  '160' => array ('x' => 467, 'y' => 29, 'value' => 0),
  '161' => array ('x' => 276, 'y' => 42, 'value' => 0),
  '162' => array ('x' => 276, 'y' => 66, 'value' => 0),
  '163' => array ('x' => 298, 'y' => 63, 'value' => 0),
  '164' => array ('x' => 326, 'y' => 52, 'value' => 0),
  '165' => array ('x' => 353, 'y' => 51, 'value' => 0),
  '166' => array ('x' => 380, 'y' => 51, 'value' => 0),
  '167' => array ('x' => 408, 'y' => 52, 'value' => 0),
  '168' => array ('x' => 437, 'y' => 52, 'value' => 0),
  '169' => array ('x' => 464, 'y' => 66, 'value' => 0),
  
  '170' => array ('x' => 465, 'y' => 93, 'value' => 0),
  '171' => array ('x' => 464, 'y' => 110, 'value' => 0),
  '172' => array ('x' => 425, 'y' => 91, 'value' => 0),
  '173' => array ('x' => 423, 'y' => 130, 'value' => 0),
  '174' => array ('x' => 466, 'y' => 160, 'value' => 0),
  '175' => array ('x' => 435, 'y' => 158, 'value' => 0),
  '176' => array ('x' => 417, 'y' => 161, 'value' => 0),
  '177' => array ('x' => 388, 'y' => 159, 'value' => 0),
  '178' => array ('x' => 361, 'y' => 159, 'value' => 0),
  '179' => array ('x' => 334, 'y' => 159, 'value' => 0),
  
  '180' => array ('x' => 270, 'y' => 175, 'value' => 0),
  '181' => array ('x' => 295, 'y' => 175, 'value' => 0),
  '182' => array ('x' => 270, 'y' => 194, 'value' => 0),
  '183' => array ('x' => 295, 'y' => 194, 'value' => 0),
  '184' => array ('x' => 272, 'y' => 212, 'value' => 0),
  '185' => array ('x' => 298, 'y' => 213, 'value' => 0),
  '186' => array ('x' => 434, 'y' => 213, 'value' => 0),
  '187' => array ('x' => 471, 'y' => 214, 'value' => 0),
  '188' => array ('x' => 459, 'y' => 198, 'value' => 0),
  '189' => array ('x' => 363, 'y' => 66, 'value' => 0),
  
  '190' => array ('x' => 417, 'y' => 65, 'value' => 0),
  '191' => array ('x' => 352, 'y' => 175, 'value' => 0),
  '192' => array ('x' => 408, 'y' => 178, 'value' => 0),
  '193' => array ('x' => 455, 'y' => 175, 'value' => 0),
  '194' => array ('x' => 324, 'y' => 174, 'value' => 0),
  '195' => array ('x' => 334, 'y' => 67, 'value' => 0),
  '196' => array ('x' => 390, 'y' => 65, 'value' => 0),
  '197' => array ('x' => 381, 'y' => 174, 'value' => 0),
  '198' => array ('x' => 438, 'y' => 195, 'value' => 0),
  '199' => array ('x' => 442, 'y' => 70, 'value' => 0),
  
  
  
  //GND floor
  '200' => array ('x' => 586, 'y' => 47, 'value' => 0),
  '201' => array ('x' => 603, 'y' => 48, 'value' => 0),
  '202' => array ('x' => 620, 'y' => 49, 'value' => 0),
  '203' => array ('x' => 637, 'y' => 48, 'value' => 0),
  '204' => array ('x' => 654, 'y' => 48, 'value' => 0),
  '205' => array ('x' => 588, 'y' => 63, 'value' => 0),
  '206' => array ('x' => 604, 'y' => 62, 'value' => 0),
  '207' => array ('x' => 621, 'y' => 62, 'value' => 0),
  '208' => array ('x' => 638, 'y' => 63, 'value' => 0),
  '209' => array ('x' => 654, 'y' => 62, 'value' => 0),
  
  '210' => array ('x' => 669, 'y' => 49, 'value' => 0),
  '211' => array ('x' => 669, 'y' => 64, 'value' => 0),
  
);


$stmt = $db->prepare("SELECT * FROM observation WHERE datetime>=? AND datetime<=? AND qk='Distance' ORDER by id_sensor,datetime;");
//$stmt = $db->prepare("SELECT * FROM prediction WHERE hour_offset=1;");
$stmt->bindValue(1,$datetimeBegin,SQLITE3_TEXT);
$stmt->bindValue(2,$datetimeEnd,SQLITE3_TEXT);

$results = $stmt->execute();

$lastSensor=0;
$distanceVariations=0;
$lastValue=0;

while ($row = $results->fetchArray()) {
    //check if it is a number
    $currentSensor=$row['id_sensor'];
    $currentValue=$row['value'];
    
    if (!is_int($currentSensor) || is_null($currentValue) )
        continue;
      
    if ($currentSensor != $lastSensor && $lastSensor != 0) {
        //write the variations
        //if this sensor is on the list
        if (array_key_exists($lastSensor, $sensorPosition)) {
            //echo ("<br>".$lastSensor.":". $distanceVariations); 
            $sensorPosition[$lastSensor]['value']=$distanceVariations;
        }
        
        //reset variables
        $distanceVariations=0;
    }

    if ($currentSensor == $lastSensor) { //same sensor, measure variations
            $distanceVariations += abs($currentValue - $lastValue);
    }
    
    $lastSensor=$currentSensor;
    $lastValue=$currentValue;        

}


$db->close();


echo ("{");
echo("\"max\": 10,");
echo ("\"data\":[");

$firstElement=true;

foreach($sensorPosition as $i){
    //echo("<br>".$i['x'].",".$i['y'].": ".$i['value']);
    if ($firstElement)
        $firstElement=false;
    else
        echo ",";
    
    echo ("{\"x\":" . $i['x'] . ", \"y\":" . $i['y'] . ", \"value\": " . $i['value'] . "}");
}


echo ("]");

echo ("}");

    
?>