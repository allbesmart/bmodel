function smallChart(parent_div_id,chartType,chartTitle) {
    this.jQueryParentDiv=$("#"+parent_div_id);
    div_width=this.jQueryParentDiv.width();
    div_height=this.jQueryParentDiv.height();
 
    this.canvas = document.createElement('canvas');
    this.canvas.width = div_width;
    this.canvas.height = div_height;
    
    this.chartTitle=chartTitle;
    
    this.jQueryParentDiv.append(this.canvas);
    this.ctx = this.canvas.getContext('2d');
    
    this.chartData=null; //will contain power consumption data

    
    //this object will contain the most recent date to be shown in the chart
    this.nowDate= new Date();

    //design properties are stored in this.dP
    this.dP = {
        chartType: chartType,
        verticalTicks: 4,
        verticalTickSpacing: 0,
        verticalTickValue: 0,
        tickColor: 'grey',
        chartColor: 'orange',
        verticalPixelsPerUnit: 0.0,
        horizontalMode: 'fixed-tick-spacing', //'fixed-tick-number' or 'fixed-tick-spacing'
        horizontalTicks: 24,
        horizontalTickSpacing: 2,
        width: div_width,
        height: div_height,
        bottomMargin: 14,
        topMargin: 1,      
        rightMargin: 1,      
        leftMargin: 16
    };
     

   //=================================================================================
    //this method sets the chart's line/bar/fill color
    this.setChartColor = function(color) {
        this.dP.chartColor=color;
    }
    
    //=================================================================================
    //this method sets the date that will be the most recent date to be shown in the chart
    this.setNowDate = function(dateString) {
        this.nowDate=new Date(dateString);
        this.nowDate.setMinutes(0);
        this.nowDate.setSeconds(0);
        //console.log(this.nowDate);
    }

     
    //=================================================================================
    //this method sets the horizontal tick spacing
    //automatically changes the number of horizontal ticks
    this.setHorizontalTickSpacing = function(pixelsPerTick) {
        this.dP.horizontalMode='fixed-tick-spacing';
        this.dP.horizontalTickSpacing=pixelsPerTick;
        this.dP.horizontalTicks=(this.dP.width-this.dP.leftMargin) / this.dP.horizontalTickSpacing;
    }


    //=================================================================================
    //this method returns the design properties of this object
    this.getDesignProperties = function() {
        return this.dP;
    }
     
     
    //=================================================================================
    //this method updates the size of the canvas to match the div size
    this.updateSize = function() {
        div_width=this.jQueryParentDiv.width();
        div_height=this.jQueryParentDiv.height();
 
        this.canvas.width = div_width;
        this.canvas.height = div_height;
        this.dP.width = div_width;
        this.dP.height = div_height;
        
        //now, calculate horizontal ticks
        if (this.dP.horizontalMode=='fixed-tick-spacing') {
            this.dP.horizontalTicks=(this.dP.width-this.dP.leftMargin) / this.dP.horizontalTickSpacing;
        }
        else if (this.dP.horizontalMode=='fixed-tick-number') {
            this.dP.horizontalTickSpacing=(this.dP.width-this.dP.leftMargin) / this.dP.horizontalTicks;
        }

     
        //console.log("New width: " + this.canvas.width);
        //console.log("this.dP.horizontalTicks: " + this.dP.horizontalTicks)
        //console.log("this.dP.horizontalTickSpacing: " + this.dP.horizontalTickSpacing)
    }
     

   //=================================================================================================
    //this method calculates the minumum and maximum value in the data
    this.calculateMinMax = function() {
        //console.log("calculating min/max for " + this.chartTitle);
        var minValue,maxValue;
        
        var datePointer= new Date(this.nowDate);
        //console.log("datePointer: " + datePointer);
        //console.log("Number of ticks: " + this.dP.horizontalTicks);
        
        for(i=0; i<=this.dP.horizontalTicks; i++) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            if (typeof(this.chartData[isoDate]) !== "undefined") { //if this date exists on the json data
                reportedValue=this.chartData[isoDate] * 1.0;  //force number
                //console.log("reported value: " + reportedValue);
                if (reportedValue) {
                    if (typeof(maxValue) == "undefined" || reportedValue>maxValue)
                        maxValue=reportedValue;
                    if (typeof(minValue) == "undefined" || reportedValue<minValue)
                        minValue=reportedValue;
                }                    
            }
            
            oldDate=new Date(datePointer);
            datePointer.setHours(datePointer.getHours()-1);
            if (oldDate.getTime() == datePointer.getTime()) { //summer time problem
                datePointer.setHours(datePointer.getHours()-2);
            }
           
        }
        
        //console.log("maxValue: " + maxValue);
        //console.log("minValue: " + minValue);
        
        return {
            minValue: minValue,
            maxValue: maxValue
        };
    };
   
 
    //=================================================================================================
    //this method calculates the vertical scale and scale labels
    this.setupScale = function() {
        if (!this.chartData) {
            this.dP.verticalTickValue= 1;
            this.dP.verticalPixelsPerUnit =  1;            
            return;
        }
        amplitudeValues=this.calculateMinMax();
        
        scaleBaseValues = [1,2,5,10,20,25];
        multiplier=1;
        maxSupportedValue=0;
        this.dP.verticalTickSpacing=Math.trunc((this.dP.height - this.dP.bottomMargin - 5)/this.dP.verticalTicks); //-10 is to 
        totalVerticalTickSpacing = this.dP.verticalTickSpacing * this.dP.verticalTicks;
        
        while (maxSupportedValue < amplitudeValues.maxValue) {
            for (x in scaleBaseValues) {
                scaleBaseValue=scaleBaseValues[x];
                maxSupportedValue=multiplier * scaleBaseValue * this.dP.verticalTicks;
                //console.log("max supportedValue = " + maxSupportedValue);
                if (maxSupportedValue >= amplitudeValues.maxValue) { //finally we have a proper scale
                    this.dP.verticalTickValue= multiplier * scaleBaseValue;
                    this.dP.verticalPixelsPerUnit =  this.dP.verticalTickSpacing / this.dP.verticalTickValue;  
                    
                    //console.log("this.dP.verticalTickValue: " +  this.dP.verticalTickValue);
                    //console.log("this.dP.verticalPixelsPerUnit: " + this.dP.verticalPixelsPerUnit);
                    return;
                }
            }
         multiplier=multiplier * 10;
        }
    };


    //=================================================================================
    //this method draws the background
    this.drawBackground = function() {

        var top = this.dP.topMargin;
        var left = this.dP.leftMargin;
        var rectWidth = this.dP.width - this.dP.leftMargin;
        var rectHeight = this.dP.height - this.dP.bottomMargin;

        var my_gradient = this.ctx.createLinearGradient(left, top, left, top + rectHeight);
        my_gradient.addColorStop(1, '#daf1f1');
        my_gradient.addColorStop(0, 'white');
        this.ctx.fillStyle = my_gradient;
        this.ctx.fillRect(left,top,rectWidth, rectHeight);
    }

    
    //=================================================================================
    //this method redraws everything
    this.update = function() {
        this.updateSize(); //check if parent div changed size
        
        this.setupScale();
        
        this.drawBackground();
        this.drawHorizontalLines();
        this.drawVerticalLines();
        this.drawVerticalLabels();
        this.drawChartTitle();
        
        //console.log("small Chart update");
        
        //draw the new data
        //if there's data to be drawn...
        if (this.chartData && typeof(this.chartData) !== "undefined") {
            if (this.dP.chartType == "line")
                this.drawChartLine();
            else if (this.dP.chartType == "bar")
                this.drawChartBar();
            else if (this.dP.chartType == "fill")
                this.drawChartFill();
        }

        this.drawBorders();
    }
    
    //=================================================================================
    //this method draws the borders
    this.drawBorders = function() {
        this.ctx.beginPath();
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = '#202020';
        this.ctx.setLineDash([0,0]); //remove dash

        //this.ctx.moveTo(this.dP.leftMargin,0)
        //this.ctx.lineTo(this.dP.leftMargin,this.dP.height  - this.dP.bottomMargin);
        //this.ctx.lineTo(this.dP.width,this.dP.height - this.dP.bottomMargin);
        
        this.ctx.rect(this.dP.leftMargin,this.dP.topMargin,this.dP.width - this.dP.leftMargin - this.dP.rightMargin,this.dP.height  - this.dP.bottomMargin - this.dP.topMargin);
        this.ctx.stroke();
        
    }

    //=================================================================================
    //this method draws the horizontal lines
    this.drawHorizontalLines = function() {
        
        this.ctx.lineWidth = 0.4;
        this.ctx.strokeStyle = this.dP.tickColor;
        this.ctx.setLineDash([1,1]);
        
        bottom = this.dP.height  - this.dP.bottomMargin;

        for(i=1; i<=this.dP.verticalTicks; i++) {
            this.ctx.beginPath();
            
            verticalPosition= bottom - this.dP.verticalTickSpacing * i + 0.1;
            this.ctx.moveTo(this.dP.leftMargin,verticalPosition)
            this.ctx.lineTo(this.dP.width,verticalPosition);
            this.ctx.stroke();
        }
    }

    //=================================================================================
    //this method draws the vertical lines
    this.drawVerticalLines = function() {
        //console.log("drawVerticalLines, this.dP.horizontalTicks:" + this.dP.horizontalTicks);
        
        //console.log(this.nowDate);
        var datePointer=new Date(this.nowDate);
        //console.log(datePointer);
        
        for(i=0; i<this.dP.horizontalTicks; i++) {
            this.ctx.beginPath();
            this.ctx.lineWidth = 0.4;
            this.ctx.strokeStyle = this.dP.tickColor;
            this.ctx.setLineDash([1,1]);
            
            xPosition=this.dP.width - i * this.dP.horizontalTickSpacing + 0.1;
            
            //this.ctx.moveTo(xPosition,0)
            //this.ctx.lineTo(xPosition,this.dP.height - this.dP.bottomMargin);
            //this.ctx.stroke();
            
            //if it is an day tick, draw it
            if (datePointer.getHours() == 0) {
                //console.log("Day tick");
                //console.log(datePointer);
                this.ctx.beginPath();
                this.ctx.lineWidth = 1;
                this.ctx.strokeStyle = this.dP.tickColor;
                this.ctx.setLineDash([1,1]);

                this.ctx.moveTo(xPosition,this.dP.topMargin)
                this.ctx.lineTo(xPosition,this.dP.height - this.dP.bottomMargin + 6);
                this.ctx.stroke();
            }
            
            //if it is noon, write the date
            if (datePointer.getHours() == 12) {
                //console.log("noon");
                //console.log(datePointer);

                this.ctx.font = '6pt Verdana';
                this.ctx.textAlign = 'center';
                this.ctx.fillStyle = '#303030';
                this.ctx.textBaseline = 'top';

                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var textualDate= monthNames[datePointer.getMonth()] + ' ' + datePointer.getDate();;
                textY=this.dP.height - this.dP.bottomMargin + 3;
                this.ctx.fillText(textualDate,xPosition,textY);               
            }


            oldDate=new Date(datePointer);
            datePointer.setHours(datePointer.getHours()-1);
            if (oldDate.getTime() == datePointer.getTime()) { //summer time problem
                datePointer.setHours(datePointer.getHours()-2);
            }
        }
    }
 
 
    //=================================================================================================
    //this method draws the vertical scale labels
    this.drawVerticalLabels = function() {

        //style for text labels
        this.ctx.font = '7pt Calibri';
        this.ctx.textAlign = 'right';
        this.ctx.fillStyle = 'black';
        this.ctx.textBaseline = 'middle';

        var secLineVerticalPosition= this.dP.height - this.dP.bottomMargin - 0.5; //0.5 trick to avoid antialiasing
        secLineValue=0;
        for(i=0; i<= this.dP.verticalTicks; i++) {
            this.ctx.fillText(''+secLineValue, this.dP.leftMargin - 3, secLineVerticalPosition);
            secLineValue+=this.dP.verticalTickValue;
            secLineVerticalPosition -= this.dP.verticalTickSpacing;
        }

    }
 
    //=================================================================================================
    //this method loads data from the server and draws the graph
    this.dataArrived = function(data) {
        //console.log("smallChart data arrived...");
        this.chartData=data;
        
        this.update();

    };
    
    //=================================================================================================
    //this method draws the predicted power graph line
    this.drawChartLine = function() {
        //some calculations
        //console.log("drawChartLine");
        if (!this.chartData)
            return;
        
        graphArea_top=0;
        graphArea_bottom=this.dP.height - this.dP.bottomMargin;
        graphArea_left=this.dP.leftMargin;
        graphArea_right=this.dP.width;

        //draw from dateEnd to dateStart
        var datePointer= new Date(this.nowDate);
        //console.log("datePointer: " + datePointer);
        //console.log("nowDate: " + this.nowDate);
        
        //line style
        this.ctx.setLineDash([]); //remove dash
        this.ctx.strokeStyle = this.dP.chartColor;
        this.ctx.lineWidth = 2;
        
        var firstPoint=1;
        var drawingNoDataZone=0;
        pointHorizontalPosition = graphArea_right;
        var pointVerticalPosition,lastPointHorizontalPosition,lastPointVerticalPosition;
        
        firstPointHorizontalPosition=pointHorizontalPosition;
        lastPointHorizontalPosition=pointHorizontalPosition;
        lastPointVerticalPosition=graphArea_bottom;
        

        this.ctx.beginPath();
      
        while ( pointHorizontalPosition >= graphArea_left ) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            
            if (typeof(this.chartData[isoDate]) !== "undefined") { //if this date exists on the json data
                //console.log("Drawing data point for " + isoDate + " with value: " + this.chartData[isoDate]);
                
                reportedValue=this.chartData[isoDate] * 1.0;
                if (!reportedValue) { //if there is no data (no fiesta data for this datetime)
                    //console.log("empty");
                    if (!drawingNoDataZone) {//if its a transition between a data and "no data" zone
                        this.ctx.stroke();
                        this.ctx.beginPath();
                        this.ctx.setLineDash([3,3]); //create a dash
                        if (!firstPoint)
                            this.ctx.moveTo(lastPointHorizontalPosition,lastPointVerticalPosition);
                    }
                    drawingNoDataZone=1;                   
                }
                else { //else, if there is data
                    pointVerticalPosition= graphArea_bottom - reportedValue * this.dP.verticalPixelsPerUnit;
                    
                    if (drawingNoDataZone) {
                        if (firstPoint)
                            this.ctx.lineTo(firstPointHorizontalPosition,pointVerticalPosition);
                        this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);
                        this.ctx.stroke();
                        this.ctx.beginPath();
                        drawingNoDataZone=0;
                        firstPoint=1;
                        this.ctx.setLineDash([]); //remove dash
                    }
                
                    if (firstPoint) {
                        this.ctx.moveTo(pointHorizontalPosition,pointVerticalPosition);
                        firstPoint=0;
                    }
                    else
                        this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);
                    
                }
                lastPointVerticalPosition=pointVerticalPosition;
                lastPointHorizontalPosition=pointHorizontalPosition;
            }

            pointHorizontalPosition -= this.dP.horizontalTickSpacing ;
            
            oldDate=new Date(datePointer);
            datePointer.setHours(datePointer.getHours()-1);
            if (oldDate.getTime() == datePointer.getTime()) { //summer time problem
                datePointer.setHours(datePointer.getHours()-2);
                pointHorizontalPosition -= this.dP.horizontalTickSpacing ;
            }

        }
        //finaly draw the lines
         //if the first zone of the chart has no data...
        if (drawingNoDataZone) {
            this.ctx.lineTo(lastPointHorizontalPosition,lastPointVerticalPosition);
            this.ctx.stroke();
        }
        else
            this.ctx.stroke();
   };   
    
    
    //=================================================================================================
    //this method draws the predicted power graph bars
    this.drawChartBar = function() {
        //some calculations
        //console.log("drawChartBar");
        if (!this.chartData)
            return;

        graphArea_top=0;
        graphArea_bottom=this.dP.height - this.dP.bottomMargin;
        graphArea_left=this.dP.leftMargin;
        graphArea_right=this.dP.width;

        //draw from dateEnd to dateStart
        var datePointer= new Date(this.nowDate);
        //console.log("datePointer: " + datePointer);
        
        //line style
        this.ctx.setLineDash([]); //remove dash
        this.ctx.fillStyle=this.dP.chartColor;
        this.ctx.lineWidth = 1;
        
        pointHorizontalPosition = graphArea_right;

        while ( pointHorizontalPosition >= graphArea_left ) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            
            if (typeof(this.chartData[isoDate]) !== "undefined") {
                //console.log("Drawing data point for " + isoDate);
                reportedValue=this.chartData[isoDate] * 1.0;
                if (reportedValue) { //if there is data
                
                    //console.log("reportedValue: " + reportedValue);
                    pointVerticalPosition= graphArea_bottom - reportedValue * this.dP.verticalPixelsPerUnit; 
                
                    rectTop=pointVerticalPosition;
                    rectLeft=pointHorizontalPosition-this.dP.horizontalTickSpacing/3;
                    rectHeight=graphArea_bottom - rectTop;
                    rectWidth=this.dP.horizontalTickSpacing*2/3,rectHeight;
                
                    if(rectLeft >= graphArea_left) //do not draw if it is outside the graph area...
                        this.ctx.fillRect(rectLeft,rectTop,rectWidth,rectHeight);
                }
            }

            pointHorizontalPosition -= this.dP.horizontalTickSpacing ;
 
            oldDate=new Date(datePointer);
            datePointer.setHours(datePointer.getHours()-1);
            if (oldDate.getTime() == datePointer.getTime()) { //summer time problem
                datePointer.setHours(datePointer.getHours()-2);
                pointHorizontalPosition -= this.dP.horizontalTickSpacing ;
            }
        }
    };   
    

    //=================================================================================================
    //this method draws the chart as a filled chart
    this.drawChartFill = function() {
        //some calculations
        
        //console.log("drawChartFill");
        
        graphArea_top=0;
        graphArea_bottom=this.dP.height - this.dP.bottomMargin;
        graphArea_left=this.dP.leftMargin;
        graphArea_right=this.dP.width;

        //draw from dateEnd to dateStart
        var datePointer= new Date(this.nowDate);
        //console.log("datePointer: " + datePointer);
        
        //line style
        this.ctx.setLineDash([]); //remove dash
        //this.ctx.strokeStyle = 'orange';
        this.ctx.strokeStyle = this.dP.chartColor;
        this.ctx.lineWidth = 2;
        
        var firstPoint=1;
        pointHorizontalPosition = graphArea_right;
        
        firstPointHorizontalPosition=pointHorizontalPosition;
        lastPointHorizontalPosition=pointHorizontalPosition;
        lastPointVerticalPosition=graphArea_bottom;

        this.ctx.beginPath();
      
        while ( pointHorizontalPosition >= graphArea_left ) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            
            if (typeof(this.chartData[isoDate]) !== "undefined") {
                //console.log("Drawing data point for " + isoDate);
                reportedValue=this.chartData[isoDate] * 1.0;
                if (reportedValue) { //if there is data
                    pointVerticalPosition= graphArea_bottom - reportedValue * this.dP.verticalPixelsPerUnit; 
                
                    if (firstPoint) {
                        this.ctx.moveTo(firstPointHorizontalPosition,pointVerticalPosition);
                        this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);
                        firstPoint=0;
                    }
                    else
                        this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);

                    lastPointVerticalPosition=pointVerticalPosition;
                }

            }

            lastPointHorizontalPosition=pointHorizontalPosition
            pointHorizontalPosition -= this.dP.horizontalTickSpacing ;

            oldDate=new Date(datePointer);
            datePointer.setHours(datePointer.getHours()-1);
            if (oldDate.getTime() == datePointer.getTime()) { //summer time problem
                datePointer.setHours(datePointer.getHours()-2);
                pointHorizontalPosition -= this.dP.horizontalTickSpacing ;
            }
        }

        //close the chart
        this.ctx.lineTo(lastPointHorizontalPosition,lastPointVerticalPosition);
        this.ctx.lineTo(lastPointHorizontalPosition,graphArea_bottom);
        this.ctx.lineTo(graphArea_right,graphArea_bottom);

        var my_gradient = this.ctx.createLinearGradient(0, 0, 0, graphArea_bottom - graphArea_top);
        my_gradient.addColorStop(1, "#FFFFFF");
        my_gradient.addColorStop(0, this.dP.chartColor);
        this.ctx.fillStyle = my_gradient;

        this.ctx.closePath();
        this.ctx.fill();
        
        //now, draw a line above...
        this.drawChartLine();
        
    };   
    
    //=================================================================================================
    //this method draws the chart title
    this.drawChartTitle = function() {
        this.ctx.font = '10pt Calibri';
        this.ctx.textAlign = 'center';
        this.ctx.fillStyle = '#303030';
        this.ctx.textBaseline = 'middle';

        textX=this.canvas.width/2;
        textY=7;
        this.ctx.fillText(this.chartTitle,textX,textY);
    }
     
  
};