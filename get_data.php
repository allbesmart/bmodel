<?php

$testbed=htmlspecialchars($_GET["testbed"]);
$datetimeBegin=htmlspecialchars($_GET["dateBegin"]);
$datetimeEnd=htmlspecialchars($_GET["dateEnd"]);
$dataSet=htmlspecialchars($_GET["dataSet"]); //this parameter must match the column name of the database

if ($testbed=='SMARTICS')
    $db = new SQLite3('/home/eiot/smartics.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='REALDC')
    $db = new SQLite3('/home/eiot/realdc.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='ADREAM')
    $db = new SQLite3('/home/eiot/adream.sqlite3',SQLITE3_OPEN_READONLY);
else
    die("Unknown testbed: ".$testbed);



$stmt = $db->prepare("SELECT * FROM observation  WHERE datetime>=? AND datetime<=? ORDER BY datetime DESC;");
//$stmt->bindValue(1,$dataSet,SQLITE3_TEXT);
$stmt->bindValue(1,$datetimeBegin,SQLITE3_TEXT);
$stmt->bindValue(2,$datetimeEnd,SQLITE3_TEXT);

$results = $stmt->execute();

echo ("{");
$firstData=true;

while ($row = $results->fetchArray()) {
    if ($firstData == false)
        echo (",");
    else
        $firstData=false;
    
    echo("\"");
    echo($row['datetime']);
    echo("\": ");

    echo("\"");
    echo($row[$dataSet]);
    echo("\"");
}

echo ("}");
    
?>