import json
import sqlite3 as lite
import sys
import datetime
import logging

con = None
logging.basicConfig(filename='update_sensor_information.log',format='%(asctime)s %(message)s',level=logging.DEBUG)

try:

    print ('updating sensor information...')

    con=lite.connect('energy-iot.sqlite3')
    cur=con.cursor()

    #get all the sensors
    cur.execute("SELECT id_sensor FROM sensor;")
    all_sensors = cur.fetchall()
    numberSensors=len(all_sensors)
    
    print('Found ' + str(numberSensors) +' sensors')
    
    for sensor in all_sensors:
        id_sensor=sensor[0]
        
        cur.execute("SELECT DISTINCT qk, unit FROM observation WHERE id_sensor=?",(id_sensor,))
        all_types = cur.fetchall()
        numberTypes=len(all_types)
        #print('Distinct types: ' + str(numberTypes))
        if (numberTypes == 1):
            qk=all_types[0][0];
            unit=all_types[0][1];
            msg='id_sensor: ' + str(id_sensor) + ' , qk: ' + qk + ', unit: ' + unit
            print msg;
            logging.debug(msg)
            
            cur.execute("UPDATE sensor SET qk=?, unit=? WHERE id_sensor=?",(qk,unit,id_sensor,))
            con.commit()
        else:
            msg='id_sensor: ' + str(id_sensor) + ' has incoherent observations'
            print msg
            logging.debug(msg)


    sys.exit(1)

    

except lite.Error, e:
    print ('Error %s:' % e.args[0])
    sys.exit(1)

finally:
    if con:
        con.close()



