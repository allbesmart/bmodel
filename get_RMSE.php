<?php

$testbed=htmlspecialchars($_GET["testbed"]);
$datetimeBegin=htmlspecialchars($_GET["dateBegin"]);
$datetimeEnd=htmlspecialchars($_GET["dateEnd"]);
$predictionMethod=htmlspecialchars($_GET["predictionMethod"]);
$predictedHours=htmlspecialchars($_GET["predictedHours"]);

//echo "opening database...";
//echo "opening database...";
//echo $datetimeBegin;
//echo "<br>";
//echo $datetimeEnd;


if ($testbed=='SMARTICS')
    $db = new SQLite3('/home/eiot/smartics.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='REALDC')
    $db = new SQLite3('/home/eiot/realdc.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='ADREAM')
    $db = new SQLite3('/home/eiot/adream.sqlite3',SQLITE3_OPEN_READONLY);
else
    die("Unknown testbed: ".$testbed);


//--------------------------------------------------------------------------------------
//get the power readings for this time period
$powerReadings=getPowerValues($db,$datetimeBegin,$datetimeEnd);

//print_r($powerReadings);

//echo ("<br>");

echo ("{");
echo ("\"datetimeBegin\": \"$datetimeBegin\"");
echo (",\"datetimeEnd\": \"$datetimeEnd\"");

$maxValue=0;

for($i=1; $i<=$predictedHours; $i++) {
    //get the predicted values for this method
    //echo("<br> ================ Calculating for $i offset Hours ===========================");
    $predictedValues=getPredictedValues($db,$predictionMethod,$datetimeBegin,$datetimeEnd,$i);
    //print_r($predictedValues);
    
    //now, calculate NMSE for this hour offset
    $nmse=calculateRMSE($powerReadings,$predictedValues,$datetimeBegin,$datetimeEnd);
    
    if($nmse > $maxValue)
        $maxValue=$nmse;
    
    if ( is_numeric($nmse) ) {
        echo(",\"$i\":");
        echo("\"$nmse\"");
    }
}

//no power, no RMSE
if (empty($powerReadings)) {
    for($i=1; $i<=$predictedHours; $i++) {
        echo(",\"$i\": 0");
    }
}

echo(",\"maxValue\" : \"$maxValue\"");
echo("}");
$db->close();








//============================================== end of main script ================================================


//--------------------------------------------------------------------------------------
function calculateRMSE($power,$predicted,$dateBegin,$dateEnd) {
    $dtBegin = new DateTime($dateBegin);
    $dtEnd = new DateTime($dateEnd);

    $dtPointer = new DateTime($dateBegin);
    $oneHour=new DateInterval('PT01H');
    
    //calculating RMSE
    $sumSquarePminusM=0;
    $n=0;
    
    while ($dtPointer <= $dtEnd) {
        $isoDtPointer=$dtPointer->format('Y-m-d')."T".$dtPointer->format('H').":00:00Z";
        //echo("<br>Processing date: ".$isoDtPointer);
        if (array_key_exists($isoDtPointer, $power) && array_key_exists($isoDtPointer, $predicted) && $power[$isoDtPointer] && $predicted[$isoDtPointer]) {
                $M=$power[$isoDtPointer];
                $P=$predicted[$isoDtPointer];
                
                //echo("<br>isoDtPointer: $isoDtPointer, M: $M, P: $P, sumM: $sumM, sumP: $sumP");
                
                $P_minus_M = $P - $M;
                //echo("<br>-------P_minus_M: ".$P_minus_M);
                $squared_P_minus_M = pow($P_minus_M, 2);
                //echo("<br>-------squared_P_minus_M: ".$squared_P_minus_M);
                $sumSquarePminusM += $squared_P_minus_M;
                
                $n++;
                
        }

        $dtPointer->add($oneHour);
    }
    
    if ($n==0 )
        return 0;
    
    //finaly calculate RMSE
    $RMSE = sqrt ( $sumSquarePminusM / $n ); 

    return $RMSE;
}





//--------------------------------------------------------------------------------------
function getPredictedValues($db,$predID,$dateBegin,$dateEnd,$hourOffset) {
    //adjust the dates back, according with $hourOffset. examples: 2 hours predictions to the current time were made 2 hours ago
    $predictionDateBegin = new DateTime($dateBegin);
    $predictionDateEnd = new DateTime($dateEnd);

    //subtract time, because the prediction was made before
    $oneHour=new DateInterval('PT01H');
    for($j=0; $j<$hourOffset; $j++) {
        $predictionDateBegin->sub($oneHour);
        $predictionDateEnd->sub($oneHour);
    }
    
    $isoDateBegin=$predictionDateBegin->format('Y-m-d')."T".$predictionDateBegin->format('H').":00:00Z";
    $isoDateEnd=$predictionDateEnd->format('Y-m-d')."T".$predictionDateEnd->format('H').":00:00Z";
    
    //echo("<br>Hour offset:".$hourOffset);
    //echo("<br>Prediction begin:". $isoDateBegin);
    //echo("<br>Prediction end:".$isoDateEnd);
 
    $stmt = $db->prepare("SELECT datetime,predicted_value FROM prediction WHERE predition_method_id=? AND hour_offset=? AND datetime>=? AND datetime<=? ORDER BY datetime;");
    $stmt->bindValue(1,$predID,SQLITE3_TEXT);
    $stmt->bindValue(2,$hourOffset,SQLITE3_TEXT);
    $stmt->bindValue(3,$isoDateBegin,SQLITE3_TEXT);
    $stmt->bindValue(4,$isoDateEnd,SQLITE3_TEXT);

    $results = $stmt->execute();
    $resArray = array();
    
    while ($row = $results->fetchArray()) {
        $databaseDate=new DateTime($row['datetime']);
        $isoDate=$databaseDate->format('Y-m-d')."T".$databaseDate->format('H').":00:00Z";
        //echo("<br>Date before add $hourOffset hours: $isoDate");

        //add time, because the prediction is for some hours later
        for($j=0; $j<$hourOffset; $j++) {
            $databaseDate->add($oneHour);
        }

        $isoDate=$databaseDate->format('Y-m-d')."T".$databaseDate->format('H').":00:00Z";
        
        //echo("<br>Date after adding $hourOffset hours: $isoDate");

        $resArray[$isoDate] = $row['predicted_value'];
    }

    return $resArray;

}
    
 
    
    
//--------------------------------------------------------------------------------------
function getPowerValues($db,$dateBegin,$dateEnd) {

    $stmt = $db->prepare("SELECT datetime,total_power FROM observation WHERE datetime>? AND datetime<=? ORDER BY datetime;");
    $stmt->bindValue(1,$dateBegin,SQLITE3_TEXT);
    $stmt->bindValue(2,$dateEnd,SQLITE3_TEXT);

    $results = $stmt->execute();
    $resArray = array();
    
    while ($row = $results->fetchArray()) {
        $resArray[$row['datetime']] = $row['total_power'];
    }

    return $resArray;
}
    
    
?>