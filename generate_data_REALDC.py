import json
import sqlite3 as lite
import sys
import datetime

con1 = None
con2 = None
#change this for other testbeds
observationsDatabaseName='energy-iot.sqlite3'
digestDatabaseName='realdc.sqlite3'
testbed_latitude=52.254

try:
    numberArguments=len(sys.argv)
    #print 'Number of arguments:', numberArguments, 'arguments.'
    #print 'Argument List:', str(sys.argv)    
    
    if numberArguments>=3: #update results for the given period
        argDatetimeBeginStr=sys.argv[1]
        argDatetimeEndStr=sys.argv[2]
        dateTimeBegin = datetime.datetime.strptime(argDatetimeBeginStr,"%Y-%m-%dT%H:%M:%SZ")
        dateTimeEnd = datetime.datetime.strptime(argDatetimeEndStr,"%Y-%m-%dT%H:%M:%SZ")
    else: #update results for the last week
        dateTimeEnd = datetime.datetime.now()
        dateTimeEnd = dateTimeEnd + datetime.timedelta(hours=1)  #try the next hour (if the cron runs at 55 minutes, for example...
        dateTimeEnd=dateTimeEnd.replace(minute=0,second=0,microsecond=0)
        dateTimeBegin = dateTimeEnd - datetime.timedelta(days=7)
        #print(dateTimeBegin)
        #print(dateTimeEnd)

    con1=lite.connect(observationsDatabaseName)
    cur1=con1.cursor()

    con2=lite.connect(digestDatabaseName)
    cur2=con2.cursor()
    

    #print dateTimeBegin
    #print dateTimeEnd
    
    dateTimeBegin=dateTimeBegin.replace(minute=0,second=0)
    dateTimeEnd=dateTimeEnd.replace(minute=0,second=0)
    
    print 'Generating data from ' + str(dateTimeBegin) + ' to ' + str(dateTimeEnd)
    
    dateTime=dateTimeBegin
    
    while dateTime<dateTimeEnd:
        periodBegin=dateTime.strftime("%Y-%m-%dT%H:%M:%SZ")
        
        #avancar uma hora, para calcular as medias da ultima hora
        dateTime = dateTime + datetime.timedelta(minutes=60)
        periodEnd=dateTime.strftime("%Y-%m-%dT%H:%M:%SZ")
        
        #se este periodo de uma hora ultrapassa a datahora final
        if dateTime>dateTimeEnd:
            break
        
        print 'Writing data for ' + periodEnd
        
        #now, calculate total power
        cur1.execute("SELECT AVG(value) FROM observation WHERE lat=? AND datetime>? AND datetime<? AND qk='ActivePower' AND id_sensor==722;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            totalPower=queryResult[0]
            #print("Total Power: " + str(totalPower))
        else:
            totalPower=None

        #now, calculate average internal humidity
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='RelativeHumidity';",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            relativeHumidity=queryResult[0]
            #print("Relative Humidity: " + str(relativeHumidity))
        else:
            relativeHumidity= None

        #now, get the internal temperature
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='AirTemperature' AND id_sensor=601;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            interiorTemperature=queryResult[0]
            #print("interior Temperature: " + str(interiorTemperature))
        else:
            interiorTemperature= None

        #now, get the external temperature
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='AirTemperature' AND id_sensor=1944;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            exteriorTemperature=queryResult[0]
            #print("exterior Temperature: " + str(exteriorTemperature))
        else:
            exteriorTemperature= None
            
        #now, get the rainfall
        cur1.execute("SELECT SUM(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='Rainfall' AND id_sensor=1949;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            rainfall=queryResult[0]
            #print("rainfall: " + str(rainfall))
        else:
            rainfall= None

        #now, get the water temperature circuit 1
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='WaterTemperature' AND id_sensor=599;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            waterTemperature1=queryResult[0]
            #print("waterTemperature1: " + str(waterTemperature1))
        else:
            waterTemperature1= None

        #now, get the water temperature circuit 2
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='WaterTemperature' AND id_sensor=616;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            waterTemperature2=queryResult[0]
            #print("waterTemperature2: " + str(waterTemperature2))
        else:
            waterTemperature2= None
            
        #now, get the windchill
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='WindChill' AND id_sensor=1935;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            windChill=queryResult[0]
            #print("windChill: " + str(windChill))
        else:
            windChill= None
            
        #now, get the windspeed
        cur1.execute("SELECT AVG(value) from observation WHERE lat=? AND datetime>? AND datetime<? AND qk='WindSpeed' AND id_sensor=568;",(testbed_latitude,periodBegin,periodEnd))
        queryResult = cur1.fetchone()
        if (queryResult != None and queryResult[0] != None):
            windSpeed=queryResult[0]
            #print("windSpeed: " + str(windSpeed))
        else:
            windSpeed= None
            

        #now, write it in the database
        cur2.execute("INSERT OR REPLACE INTO observation(datetime,total_power,humidity,internal_temperature,external_temperature,rainfall,water_temperature1,water_temperature2,wind_chill,wind_speed) VALUES (?,?,?,?,?,?,?,?,?,?);",(periodEnd,totalPower,relativeHumidity,interiorTemperature,exteriorTemperature,rainfall,waterTemperature1,waterTemperature2,windChill,windSpeed))
        con2.commit()
            
except lite.Error, e:
    print ('Error %s:' % e.args[0])
    sys.exit(1)

finally:
    if con1:
        con1.close()

    if con2:
        con2.close()



