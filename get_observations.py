import requests
import fiesta_auth
import json
import sqlite3 as lite
import sys

con = None

query_part1="Prefix ssn: <http://purl.oclc.org/NET/ssnx/ssn#>\n\
Prefix iotlite: <http://purl.oclc.org/NET/UNIS/fiware/iot-lite#>\n\
Prefix dul: <http://www.loa.istc.cnr.it/ontologies/DUL.owl#>\n\
Prefix geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n\
Prefix time: <http://www.w3.org/2006/time#>\n\
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n\
Prefix m3-lite: <http://purl.org/iot/vocab/m3-lite#>\n\
Prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n\
select ?sensorID (max(?ti) as ?time) ?value ?latitude ?longitude ?qk\n\
where {\n\
    ?o a ssn:Observation.\n"

query_part2="    ?o ssn:observedBy ?sensorID.\n\
    ?o ssn:observedProperty ?qkr.\n\
    ?qkr rdf:type ?qk.\n\
    ?o ssn:observationSamplingTime ?t.\n\
    ?o geo:location ?point.\n\
    ?point geo:lat ?latitude.\n\
    ?point geo:long ?longitude.\n\
    ?t time:inXSDDateTime ?ti.\n\
    ?o ssn:observationResult ?or.\n\
    ?or  ssn:hasValue ?v.\n\
    ?v dul:hasDataValue ?value.\n"
    
query_part3="\n\
} group by ?sensorID ?time ?value ?latitude ?longitude ?qk\n\
order by asc (?time)\n\
limit 10000"


try:
    con=lite.connect('energy-iot.sqlite3')
    cur=con.cursor()

    sensorNumber=0;
    
    sql="SELECT  id_sensor,unit FROM sensor;"
    cur.execute(sql)
    all_rows = cur.fetchall()
    for row in all_rows:
        sensor_id=row[0]
        unit=row[1]
        
        #obter a data da ultima leitura deste sensor
        sql="SELECT MAX(datetime) FROM observations WHERE id_sensor='" + sensor_id + "';";
        cur.execute(sql)
        res = cur.fetchone()
        if (res[0]==None):
            lastDate="1980-09-05T00:00:00.000Z"
        else:
            lastDate=res[0]
      
        sensorNumber=sensorNumber+1
        
        #controlo do sensor inicial a ir buscar...
        if sensorNumber<153:
            continue
        
               
        print("------------------------------------------")
        print("Sensor " + str(sensorNumber) + ":" + sensor_id)
        print("Getting observations since: " + lastDate)
        
        #login na plataforma
        token=fiesta_auth.login()
        if token == None:
            print "Login failure"
            break;
        
        query=query_part1
        query=query + "    Values ?sensorID {<" + sensor_id + ">}\n"
        query=query + query_part2
        query=query + "    FILTER((xsd:dateTime(?ti) > \"" + lastDate + "\"^^xsd:dateTime))\n"
        query=query + query_part3
        #print (query)

        url='https://platform.fiesta-iot.eu/iot-registry/api/queries/execute/global'
        headers={'iPlanetDirectoryPro':token,'Content-Type':'text/plain','Accept':'application/json'}
        r = requests.post(url,headers=headers,data=query)
        #print r.status_code
        
        if (r.status_code!=200):
            print ("Erro, status code=" + str(r.status_code))
            continue
        
        parsed=r.json()  
        numberObservations=0
        for c in parsed['items']:
            #print('---------------------------------------------------')

            temp=c.get('qk')
            #detetar estrutura JSON vazia...
            if (temp==None):
                print("This sensor has no observations since " + lastDate)
                continue
            
            numberObservations=numberObservations+1
             
            qk=temp.split('#')[1]
            #print(' ')
            #print ('qk:' + qk)

            temp=c.get('longitude');
            long=temp.split('^^')[0]
            long=float(long)
            #print(' ')
            #print ('long:'+ str(long))

            temp=c.get('latitude')
            lat=temp.split('^^')[0]
            lat=float(lat)
            #print(' ')
            #print ('lat:'+ str(lat))

            temp=c.get('value')
            value=temp.split('^^')[0]
            value=float(value)
            #print(' ')
            #print ('value:'+ str(value))

            temp=c.get('time')
            time=temp.split('^^')[0]
            #print(' ')
            #print ('time:'+ time)
        
            sql="INSERT INTO observations (datetime,id_sensor,qk,value,unit,lat,long) VALUES ('"\
            + time + "','" + sensor_id +"','" + qk + "','" + str(value) + "','" + unit + "'," + str(lat) +"," + str(long) +");"
            #print (sql)

            cur.execute(sql)
        
        #apos escrever as observacoes de cada sensor...
        con.commit()
        #logout para fazer refresh do login
        fiesta_auth.logout(token)
        
        print("Wrote " + str(numberObservations) + " observations for this sensor")

        


except lite.Error, e:
    print ('Error %s:' % e.args[0])
    fiesta_auth.logout(token)
    sys.exit(1)

finally:
    if con:
        con.commit()
        con.close()
        fiesta_auth.logout(token)


