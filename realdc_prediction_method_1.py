#this example shows how to implement a prediction scripts
import sys
import datetime
import random
import realdc

PREDICTION_METHOD_1 = 1

from datetime import timedelta

import tensorflow as tf
import pandas as pd
import numpy as np
from math import sqrt
from pandas import concat

PREDICTION_METHOD_1 = 1

date1 = datetime.datetime.now() - datetime.timedelta(days=30)
date1 = date1.replace(minute=0,second=0,microsecond=0)
date2 = datetime.datetime.now()
date2 = date2.replace(minute=0,second=0,microsecond=0)    

result=realdc.getBulkObservations(date1,date2)

df = pd.DataFrame(result)

Total_Power=df.loc["total_power", :]

values = Total_Power
dataframe = concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']
print(dataframe.head(5))

X = dataframe.values
train_size = int(len(X) * 0.768) 
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

def model_persistence(x):
	return x

predictions = list()
for x in test_X:
	yhat = model_persistence(x)
	predictions.append(yhat)
# test_score = mean_squared_error(test_y, predictions)
# print('Test MSE: %.3f' % test_score)
# rmse = sqrt(test_score)
# print("RMSE=",rmse, "W")

y = predictions
print(y)
print(len(y))

for i in range(0,167):
	realdc.setPrediction(date2,i+1,PREDICTION_METHOD_1,y[i]*1.0)

