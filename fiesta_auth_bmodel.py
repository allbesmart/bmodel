import requests
import time


def login():
    #print("obtendo token...")
    try:
        url='https://platform.fiesta-iot.eu/openam/json/authenticate'
        headers={'X-OpenAM-Username':'jribeiro','X-OpenAM-Password':'Fiesta-ABS.teste','Content-Type':'application/json' }
        r = requests.post(url, headers=headers,timeout=10)
        statusCode=r.status_code
        if statusCode!=200:
            return None

        #print r.status_code
        response=r.json()
        #print response
        token=response['tokenId']
        print ("login: " + str(statusCode) )
        time.sleep(10) #to avoid firewall blocking of the next request
        return token

    except requests.exceptions.RequestException as e:
        print e
        return None

    except requests.exceptions.Timeout as e:
        print e
        return None

def logout(token):
    url='https://platform.fiesta-iot.eu/openam/json/sessions/?_action=logout'
    headers={'iPlanetDirectoryPro':token}

    time.sleep(10) #to avoid firewall blocking of this request

    r = requests.post(url,headers=headers,timeout=10)
    statusCode=r.status_code
    print("logout: " + str(statusCode))
    return
