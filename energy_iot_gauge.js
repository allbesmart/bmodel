function energyGauge(parent_div_id,canvas_id,labelText) {
    this.jQueryParentDiv=$("#"+parent_div_id);

    div_width=this.jQueryParentDiv.width();
    div_height=this.jQueryParentDiv.height();
 
    this.canvas = document.createElement('canvas');
    this.canvas.width = div_width;
    this.canvas.height = div_height;
    
    this.jQueryParentDiv.append(this.canvas);
    this.ctx = this.canvas.getContext('2d');
    

    this.labelText=labelText;
 
    //design properties are stored in this.dP
    this.dP = {
        initialAngle: 0.8 * Math.PI,
        finalAngle: 2.2 * Math.PI,
        baseRadius: 30,
        centerX: 5,
        centerY: 5,
        topMargin: 2,
        bandWidth: 15,
        majorTicks: 5,
        minorTicks: 50,
        greenZoneColor: '#0092FF',
        yellowZoneColor: 'white',
        redZoneColor: 'white',
        lidColor: '#222211',
        pointerColor: '#009900',
        pointerThickness: 5
    };
    
    //values are stored in this.val
    this.val = {
        expectedValue:100,
        displayValue:0
    }
    

    //=================================================================================
    //this method changes values of the gauge
    this.updateValues = function(expectedValue, realValue) {
        this.val.expectedValue = expectedValue;
        this.val.displayValue = realValue;
        this.update();
    }
    
    //=================================================================================
    //this method draws the scale
    this.update = function() {
                
        //some calculations
        div_width=this.jQueryParentDiv.width();
        div_height=this.jQueryParentDiv.height();
        
        this.ctx.clearRect(0,0,div_width, div_height);

       
        this.dP.centerX = this.ctx.canvas.width/2;
        this.dP.centerY = this.dP.topMargin + (this.ctx.canvas.height-this.dP.topMargin)/2 ;
        //console.log("this.dP.centerY: " + this.dP.centerY);
        
        this.dP.baseRadius = this.ctx.canvas.width/2 - 10;
        
        this.dP.bandWidth = this.dP.baseRadius / 5 ;
        
        var my_gradient = this.ctx.createLinearGradient(0, 0, div_width, div_height);
        my_gradient.addColorStop(1, '#daf1f1');
        my_gradient.addColorStop(0, 'white');

        
        //circle around
        this.ctx.beginPath();
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = '#AAAAAA';
        this.ctx.fillStyle = my_gradient;
        this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.baseRadius + this.dP.bandWidth /2, 0, 2* Math.PI);
        this.ctx.fill();
        this.ctx.stroke();
        
        //base color green
        greenAngle= this.dP.initialAngle + (this.dP.finalAngle-this.dP.initialAngle)*this.val.expectedValue /100;
        this.ctx.beginPath();
        this.ctx.lineWidth = this.dP.bandWidth;
        this.ctx.strokeStyle = this.dP.greenZoneColor;
        this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.baseRadius, this.dP.initialAngle, greenAngle);
        this.ctx.stroke();

		
        //base color yellow
        yellowAngle= greenAngle + 0.5 ;
        if (yellowAngle > this.dP.finalAngle)
            yellowAngle = this.dP.finalAngle;
            
        this.ctx.beginPath();
        this.ctx.lineWidth = this.dP.bandWidth;
        this.ctx.strokeStyle = this.dP.yellowZoneColor;
        this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.baseRadius, greenAngle, yellowAngle);
        this.ctx.stroke();
        
        //base color red
        this.ctx.beginPath();
        this.ctx.lineWidth = this.dP.bandWidth;
        this.ctx.strokeStyle = this.dP.redZoneColor;
        this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.baseRadius, yellowAngle, this.dP.finalAngle);
        this.ctx.stroke();
        

        //minor ticks
        this.ctx.lineWidth = this.dP.bandWidth /2;
        this.ctx.strokeStyle = '#000000';
        anglePiece=(this.dP.finalAngle-this.dP.initialAngle)/this.dP.minorTicks;
        for (i=0; i<=this.dP.minorTicks; i++) {
            this.ctx.beginPath();
            this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.baseRadius + this.ctx.lineWidth /2, this.dP.initialAngle +i*anglePiece, this.dP.initialAngle +i*anglePiece + 0.01);
            this.ctx.stroke();
        }

        //major ticks
        this.ctx.lineWidth = this.dP.bandWidth;
        this.ctx.strokeStyle = '#606060';
        anglePiece=(this.dP.finalAngle-this.dP.initialAngle)/this.dP.majorTicks;
        for (i=0; i<=this.dP.majorTicks; i++) {
            this.ctx.beginPath();
            this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.baseRadius, this.dP.initialAngle +i*anglePiece, this.dP.initialAngle +i*anglePiece + 0.02);
            this.ctx.stroke();
        }
        
        //major labels
        //style for text labels
        this.ctx.font = '6pt Verdana';
        this.ctx.textAlign = 'center';
        this.ctx.fillStyle = '#606060';
        this.ctx.textBaseline = 'middle';
        
        textRadius= this.dP.baseRadius - this.dP.bandWidth * 1.2;
        anglePiece=(this.dP.finalAngle-this.dP.initialAngle)/this.dP.majorTicks;
        for (i=0; i<=this.dP.majorTicks; i++) {
            currentAngle=this.dP.initialAngle +i*anglePiece;
            textX=this.dP.centerX + textRadius * Math.cos(currentAngle);
            textY=this.dP.centerY + textRadius * Math.sin(currentAngle);
            label=(100*i)/this.dP.majorTicks;
            this.ctx.fillText(label,textX,textY);
        }
       
        //reading value
        this.ctx.font = '10pt Verdana';
        this.ctx.textAlign = 'center';
        this.ctx.fillStyle = 'black';
        this.ctx.textBaseline = 'middle';

        textX=this.dP.centerX;
        textY=this.dP.centerY + textRadius;
        this.ctx.fillText(this.val.displayValue + "%",textX,textY);
        


        
        //pointer
        pointerAngle= this.dP.initialAngle + (this.dP.finalAngle-this.dP.initialAngle)*this.val.displayValue /100;
        if (pointerAngle > this.dP.finalAngle + 0.2)
            pointerAngle=this.dP.finalAngle + 0.2;
        
        rightAngle=pointerAngle + Math.PI /2;
        leftAngle=pointerAngle - Math.PI /2;
        pointerSize= this.dP.baseRadius;

        this.ctx.beginPath();
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = '#AAAAAA';
        
        posX=this.dP.centerX + this.dP.pointerThickness * Math.cos(leftAngle);
        posY=this.dP.centerY + this.dP.pointerThickness * Math.sin(leftAngle);
        this.ctx.moveTo(posX,posY);

        posX=this.dP.centerX + this.dP.pointerThickness * Math.cos(rightAngle);
        posY=this.dP.centerY + this.dP.pointerThickness * Math.sin(rightAngle);
        this.ctx.lineTo(posX,posY);
        
        posX=this.dP.centerX + pointerSize * Math.cos(pointerAngle);
        posY=this.dP.centerY + pointerSize * Math.sin(pointerAngle);
        this.ctx.lineTo(posX,posY);
        this.ctx.closePath();
 
        this.ctx.stroke();
        this.ctx.fillStyle=this.dP.pointerColor;
        this.ctx.fill();

        //round lid
        this.ctx.beginPath();
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = 'white';
        this.ctx.arc(this.dP.centerX,this.dP.centerY,this.dP.pointerThickness * 2, 0, Math.PI * 2);
        this.ctx.stroke();
        this.ctx.fillStyle=this.dP.lidColor;
        this.ctx.fill();
        

        
    };  
    
    
    

    
};