#this example shows how to implement a prediction scripts
import sys
import datetime
import random
import smartics
from datetime import timedelta
import pandas as pd
import numpy as np

PREDICTION_METHOD_2 = 2

from math import sqrt
from numpy import concatenate
from pandas import DataFrame
from pandas import concat
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM

date1 = datetime.datetime.now() - datetime.timedelta(days=30)
date1 = date1.replace(minute=0,second=0,microsecond=0)
date2 = datetime.datetime.now()
date2 = date2.replace(minute=0,second=0,microsecond=0)        

#Read bulk observations of the period between the two dates
result=smartics.getBulkObservations(date1,date2)

#print(result) #resultados dos ultimos 30 dias

df = pd.DataFrame(result)

print(df.head)

#values = np.array(df.values)

Total_Power=df.loc["total_power", :] #total power, dos ultimos 30 dias
#print(df.head) 
Humidity = df.loc["humidity", :]
iTemperature = df.loc["internal_temperature", :]
human_activity = df.loc["human_activity", :]

#print(df.values[4,:])

df = pd.concat([Total_Power,Humidity,iTemperature,human_activity],axis=1)

#____________________________________________________________________
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
	n_vars = 1 if type(data) is list else data.shape[1]
	df = DataFrame(data)
	cols, names = list(), list()
	# input sequence (t-n, ... t-1)
	for i in range(n_in, 0, -1):
		cols.append(df.shift(i))
		names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
	# forecast sequence (t, t+1, ... t+n)
	for i in range(0, n_out):
		cols.append(df.shift(-i))
		if i == 0:
			names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
		else:
			names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
	# put it all together
	agg = concat(cols, axis=1)
	agg.columns = names
	# drop rows with NaN values
	if dropnan:
		agg.dropna(inplace=True)
	return agg
# 
# ensure all data is float
df = df.astype('float32')

print(len(df))

# normalize features
scaler = MinMaxScaler(feature_range=(0, 1))
scaled = scaler.fit_transform(df)
# frame as supervised learning

encoder = LabelEncoder()
# #print(values[3,:])
Total_Power = encoder.fit_transform(Total_Power)


#print(Total_Power)

reframed = series_to_supervised(scaled, 1, 1)
# #print(reframed)
# # drop columns we don't want to predict
reframed.drop(reframed.columns[[1,2,3]], axis=1, inplace=True)
# #print(reframed.head())

# #______________________________________________________
# # split into train and test sets
values = reframed.values
n_train_hours = 552
train = values[:n_train_hours, :]
test = values[n_train_hours:, :]
# split into input and outputs
train_X, train_y = train[:, :-1], train[:, -1]
test_X, test_y = test[:, :-1], test[:, -1]
 #reshape input to be 3D [samples, timesteps, features]
train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
test_X = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))
#print(train_X.shape, train_y.shape, test_X.shape, test_y.shape)

#__________________________________________________
model = Sequential()
model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
model.add(Dense(1))
model.compile(loss='mae', optimizer='adam')

history = model.fit(train_X, train_y, epochs=50, batch_size=168, validation_data=(test_X, test_y), verbose=2, shuffle=False)

# make a prediction
yhat = model.predict(test_X)
test_X = test_X.reshape((test_X.shape[0], test_X.shape[2]))
# invert scaling for forecast
inv_yhat = concatenate((yhat, test_X[:, 1:]), axis=1)
inv_yhat = scaler.inverse_transform(inv_yhat)
inv_yhat = inv_yhat[:,0]
# invert scaling for actual
test_y = test_y.reshape((len(test_y), 1))
inv_y = concatenate((test_y, test_X[:, 1:]), axis=1)
inv_y = scaler.inverse_transform(inv_y)
inv_y = inv_y[:,0]

# print(inv_y[0])
# print(len(inv_y))

for i in range(0,167):
	smartics.setPrediction(date2,i+1,PREDICTION_METHOD_2,inv_y[i])