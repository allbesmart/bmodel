var dashPredictRealDC;
var dashPredictSmartICS;

var realdcGaugeToday,realdcGaugeWeek,smarticsGaugeToday,smarticsGaugeWeek;

var realdcChartInternalTemperature,realdcChartHumidity;
var thermo1,thermo2;
var humidity1,humidity2;

var realdc_wind_speed,realdc_rainfall,realdc_water_temp;

var realdcNumberDataStreamsArrived=0;
var smarticsNumberDataStreamsArrived=0;

//===================================================================================================
//This function returns the current date and hour in ISO format
function getNow() {
    nowDate=new Date();
    
    //go back 10 minutes to give time to the prediction models to run
    nowDate.setMinutes(nowDate.getMinutes() - 10);
    
    nowDate.setMinutes(0);
    nowDate.setSeconds(0);
    
    isoDate=nowDate.toISOString().slice(0, 19) + 'Z';
    return isoDate;
}




//===================================================================================================
//This function builds the JSON request URL
function buildJSONrequestURL(testbed,variable,initialDate,requestedDays) {
    nowDate=new Date(initialDate);
    //console.log(nowDate);
    
    previousDate=new Date(nowDate);
    previousDate.setMinutes(0);
    previousDate.setSeconds(0);
    previousDate.setDate(nowDate.getDate() - requestedDays);
    
    isoNowDate=nowDate.toISOString().slice(0, 19) + 'Z';
    isoPreviousDate=previousDate.toISOString().slice(0, 19) + 'Z';

    //console.log("isoNowDate:" + isoNowDate);
    //console.log("isoPreviousDate" + isoPreviousDate);
    
    var url="get_data.php?testbed=" + testbed + "&dateBegin=" + isoPreviousDate + "&dateEnd=" + isoNowDate + "&dataSet=" + variable;
    //console.log(url);
    return url;
}


//===================================================================================================
//This function builds the JSON request URL
function buildJSONrequestPredictionURL(testbed,methodID,initialDate,requestedDays) {
    nowDate=new Date(initialDate);
    //console.log(nowDate);
    
    previousDate=new Date(nowDate);
    previousDate.setMinutes(0);
    previousDate.setSeconds(0);
    previousDate.setDate(nowDate.getDate() - requestedDays);
    
    isoNowDate=nowDate.toISOString().slice(0, 19) + 'Z';
    isoPreviousDate=previousDate.toISOString().slice(0, 19) + 'Z';

    //console.log("isoNowDate:" + isoNowDate);
    //console.log("isoPreviousDate" + isoPreviousDate);
    
    var url="get_prediction_data.php?testbed=" + testbed + "&dateBegin=" + isoPreviousDate + "&dateEnd=" + isoNowDate + "&methodID=" + methodID;
    //console.log(url);
    return url;
}

//===================================================================================================
//This function builds the JSON request URL
function buildJSONrequestPredictionURL_v2(testbed,methodID,initialDate,requestedDays) {
    nowDate=new Date(initialDate);
    //console.log(nowDate);
    
    previousDate=new Date(nowDate);
    previousDate.setMinutes(0);
    previousDate.setSeconds(0);
    previousDate.setDate(nowDate.getDate() - requestedDays);
    
    isoNowDate=nowDate.toISOString().slice(0, 19) + 'Z';
    isoPreviousDate=previousDate.toISOString().slice(0, 19) + 'Z';

    //console.log("isoNowDate:" + isoNowDate);
    //console.log("isoPreviousDate" + isoPreviousDate);
    
    var url="get_prediction_data.php?testbed=" + testbed + "&dateBegin=" + isoPreviousDate + "&dateEnd=" + isoNowDate + "&methodID=" + methodID;
    //console.log(url);
    return url;
}


//===================================================================================================
//This function builds the JSON request URL to get MSE calculations
function buildJSONrequestMSE(testbed,methodID,initialDate,requestedDays,predictedHours) {
    nowDate=new Date(initialDate);
    //console.log(nowDate);
    
    previousDate=new Date(nowDate);
    previousDate.setMinutes(0);
    previousDate.setSeconds(0);
    previousDate.setDate(nowDate.getDate() - requestedDays);
    
    isoNowDate=nowDate.toISOString().slice(0, 19) + 'Z';
    isoPreviousDate=previousDate.toISOString().slice(0, 19) + 'Z';

    //console.log("isoNowDate:" + isoNowDate);
    //console.log("isoPreviousDate" + isoPreviousDate);
    
    var url="get_NMSE.php?testbed=" + testbed + "&dateBegin=" + isoPreviousDate + "&dateEnd=" + isoNowDate + "&predictionMethod=" + methodID + "&predictedHours=" + predictedHours;
    //console.log(url);
    return url;
}







function adjustPositions() {
    //======================================================================
    //------------------- REAL DC objects ----------------------------------
    var realdcContainerDiv=$("#REALDC-DIV");
    
    var realdcTodayDiv=$("#realdc-today-div");
    
    var todayTop= realdcTodayDiv.position().top;
    var todayHeight= realdcTodayDiv.height();
    var xSeparator= realdcContainerDiv.width() - todayHeight -2;
    
    //console.log("----------------------------------------------");
    //console.log("realdc xSeparator:" + xSeparator);

    $("#realdc-today-div").width(xSeparator);
    $("#realdc-today-gauge-div").css({top: todayTop, left: xSeparator});

    var realdcNextWeekDiv=$("#realdc-next-week-div");
    var nextWeekTop= realdcNextWeekDiv.position().top;
    var nextWeekHeight= realdcNextWeekDiv.height();
    xSeparator= realdcContainerDiv.width() - nextWeekHeight -2;

    //console.log("realdc xSeparator:" + xSeparator);
    
    $("#realdc-next-week-div").width(xSeparator);
    $("#realdc-next-week-gauge-div").css({top: nextWeekTop, left: xSeparator});
    
    //console.log("realdc-today-div.width()=" + $("#realdc-today-div").width());

    
    //now the small charts
    smallChartsLeft1= 5;
    smallChartsWidth= (realdcContainerDiv.width() / 2) - 10;

    smallChartsTop=nextWeekTop + nextWeekHeight + 30;
    $("#realdc-chart-internal-temperature").css({top: smallChartsTop, left: smallChartsLeft1, width: smallChartsWidth});
    
    smallChartsTop += $("#realdc-chart-internal-temperature").height() + 20; 
    $("#realdc-chart-external-temperature").css({top: smallChartsTop, left: smallChartsLeft1, width: smallChartsWidth});

    smallChartsTop += $("#realdc-chart-external-temperature").height() + 20; 
    $("#realdc-chart-humidity").css({top: smallChartsTop, left: smallChartsLeft1, width: smallChartsWidth});
    
    smallChartsTop += $("#realdc-chart-humidity").height() + 20; 
    $("#realdc-chart-rainfall").css({top: smallChartsTop, left: smallChartsLeft1, width: smallChartsWidth});

    //2nd column
    smallChartsLeft2= (realdcContainerDiv.width() / 2) + 5 ;

    smallChartsTop=nextWeekTop + nextWeekHeight + 30;
    $("#realdc-chart-water-temperature1").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});

    smallChartsTop += $("#realdc-chart-water-temperature1").height() + 20; 
    $("#realdc-chart-water-temperature2").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});

    smallChartsTop += $("#realdc-chart-water-temperature2").height() + 20; 
    $("#realdc-chart-wind-speed").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});
    
    smallChartsTop += $("#realdc-chart-wind-speed").height() + 20; 
    $("#realdc-chart-wind-chill").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});


    
    //=====================================================================================================================
    //------------------- SMART ICS objects ----------------------------------
    var smarticsContainerDiv=$("#SMARTICS-DIV");
    
    var smarticsTodayDiv=$("#smartics-today-div");
    
    var todayTop= smarticsTodayDiv.position().top;
    var todayHeight= smarticsTodayDiv.height();
    var xSeparator= smarticsContainerDiv.width() - todayHeight -2;
    
    //console.log("smartics xSeparator:" + xSeparator);
    
    $("#smartics-today-div").width(xSeparator);
    
    $("#smartics-today-gauge-div").css({top: todayTop, left: xSeparator});
    
    //console.log("smartics-today-div.width()=" + $("#smartics-today-div").width());

    var smarticsNextWeekDiv=$("#smartics-next-week-div");
    var nextWeekTop= smarticsNextWeekDiv.position().top;
    var nextWeekHeight= smarticsNextWeekDiv.height();
    xSeparator= smarticsContainerDiv.width() - nextWeekHeight -2;
    
    $("#smartics-next-week-div").width(xSeparator);
    $("#smartics-next-week-gauge-div").css({top: nextWeekTop, left: xSeparator});

	//small charts
	smallChartsWidth= (smarticsContainerDiv.width() / 3) - 6;
    //2nd column
    smallChartsLeft2= 5 ;

    smallChartsTop=nextWeekTop + nextWeekHeight + 30;
    $("#smartics-chart-internal-temperature").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});
    
    smallChartsLeft2 += smallChartsWidth + 5;
    $("#smartics-chart-humidity").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});

    smallChartsLeft2 += smallChartsWidth + 5;
    $("#smartics-chart-human-activity").css({top: smallChartsTop, left: smallChartsLeft2, width: smallChartsWidth});

    //heatmap
    heatmapWidth = $("#smartics-heatmap").width();
	heatmapHeight = $("#smartics-heatmap").height();
	heatmapTop = smallChartsTop + $("#smartics-chart-internal-temperature").height() + 5;
	heatmapLeft = (smarticsContainerDiv.width() - heatmapWidth ) / 2;
    if (heatmapLeft < 5)
        heatmapLeft = 5;
	
    $("#smartics-heatmap").css({top: heatmapTop, left: heatmapLeft});
    
  
    
}


//===========================================================================================
function updateHeatmap(updateDate) {
    //console.log("update Interface");
    var twoHoursAgo=new Date(updateDate);
    twoHoursAgo.setHours(twoHoursAgo.getHours() - 2);
    
    isoTwoHoursAgo=twoHoursAgo.toISOString().slice(0, 19) + 'Z';
    
    var url="get_heatmap.php?dateBegin=" + isoTwoHoursAgo + "&dateEnd=" + updateDate;
    //console.log(url);
    $.getJSON(url, function (data) {
        //console.log(data);
        smarticsHeatmap.setData(data);    
    });

}




//===========================================================================================
function updateInterface(updateDate) {
    //console.log("update Interface");
    adjustPositions();
    
    updateRealdcDashboards(updateDate);
    updateSmarticsDashboards(updateDate);
    updateHeatmap(updateDate);
     
};



//===========================================================================================
function updateRealdcDashboards(updateDate) {
    
    dashPredictRealDC.setNowDate(updateDate)
    dashPredictRealDC.update();
    
    realdcNextWeekPredictor.setNowDate(updateDate);
    realdcNextWeekPredictor.update();
    realdcGetMSEforWeek();
	
	realdcTodayPredictor.setNowDate(updateDate);
	realdcTodayPredictor.update();
    realdcGetMSEforToday();
   
    realdcGaugeToday.update();
    realdcGaugeWeek.update();
    
    realdcChartInternalTemperature.setNowDate(updateDate);
    realdcChartInternalTemperature.update();

    realdcChartExternalTemperature.setNowDate(updateDate);
    realdcChartExternalTemperature.update();

    realdcChartHumidity.setNowDate(updateDate);
    realdcChartHumidity.update();

    realdcChartRainfall.setNowDate(updateDate);
    realdcChartRainfall.update();

    realdcChartWaterTemperature1.setNowDate(updateDate);
    realdcChartWaterTemperature1.update();

    realdcChartWaterTemperature2.setNowDate(updateDate);
    realdcChartWaterTemperature2.update();

    realdcChartWindSpeed.setNowDate(updateDate);
    realdcChartWindSpeed.update();

    realdcChartWindChill.setNowDate(updateDate);
    realdcChartWindChill.update();
    
}






//===========================================================================================
function updateSmarticsDashboards(updateDate) {
    dashPredictSmartICS.setNowDate(updateDate)
    dashPredictSmartICS.update();
    
    smarticsTodayPredictor.setNowDate(updateDate);
    smarticsTodayPredictor.update();
    smarticsGetMSEforToday();
	
	smarticsNextWeekPredictor.setNowDate(updateDate);
    smarticsNextWeekPredictor.update()
    smarticsGetMSEforWeek();
    
    smarticsGaugeToday.update();
    smarticsGaugeWeek.update();
    
	smarticsChartInternalTemperature.setNowDate(updateDate);
	smarticsChartInternalTemperature.update();

	smarticsChartHumidity.setNowDate(updateDate);
    smarticsChartHumidity.update();

	smarticsChartHumanActivity.setNowDate(updateDate);
    smarticsChartHumanActivity.update();   

}




//this method will create the dashboard objects for SMARTICS
function createObjectsSMARTICS(thisInstant) {
    dashPredictSmartICS=new  DashboardPredict("energy-iot-dashboard-predict-div-smartics","energy-iot-dashboard-predict-canvas-smartics");

    smarticsTodayPredictor= new nextHoursPredict("smartics-today-div");
    smarticsTodayPredictor.setNowDate(thisInstant);
    
    smarticsNextWeekPredictor= new nextDaysPredict("smartics-next-week-div");
    smarticsNextWeekPredictor.setNowDate(thisInstant);

    smarticsGaugeToday= new energyGauge("smartics-today-gauge-div");
    smarticsGaugeWeek= new energyGauge("smartics-next-week-gauge-div");

    smarticsChartInternalTemperature= new smallChart("smartics-chart-internal-temperature","fill","Internal air temperature (ºC)");
    smarticsChartInternalTemperature.setChartColor('#006633');

    smarticsChartHumidity= new smallChart("smartics-chart-humidity","fill","Internal humidity (%)");
    smarticsChartHumidity.setChartColor('#0088cc');

    smarticsChartHumanActivity= new smallChart("smartics-chart-human-activity","bar","Human presence");
    smarticsChartHumanActivity.setChartColor('#b35900');
    
    smarticsHeatmap = h337.create({
    // only container is required, the rest will be defaults
        container: document.querySelector('#heatmapContainer'),
    });
  
}


//------------------------------------------------------------------------------------------------------
//this method will 
function smarticsGetMSEforToday() {
    //=============================== RMSE & Gauge for smartics TODAY
    var rcDates=smarticsTodayPredictor.getDates();
    //console.log(rcDates);
    var isoStartDate=rcDates.startDate.toISOString().slice(0, 19) + 'Z';
    var isoNowDate=rcDates.nowDate.toISOString().slice(0, 19) + 'Z';

    var url="get_RMSE.php?testbed=SMARTICS" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=1&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		smarticsTodayPredictor.setRMSE1(data['1']);
    });
 
    var url="get_RMSE.php?testbed=SMARTICS" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=2&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		smarticsTodayPredictor.setRMSE2(data['1']);
        //update gauge
        todayConsumption=smarticsTodayPredictor.getConsumptionIndicators();
            //console.log("expectedConsumed2: " + todayConsumption.expectedConsumed2);
            //console.log("totalExpectedConsumed2: " + todayConsumption.totalExpectedConsumed2);
            //console.log("totalConsumed: " + todayConsumption.totalConsumed);
        if (todayConsumption.totalExpectedConsumed2 >0) {
            var expected=Math.trunc((todayConsumption.expectedConsumed2 / todayConsumption.totalExpectedConsumed2)*100);
            var consumed=Math.trunc((todayConsumption.totalConsumed / todayConsumption.totalExpectedConsumed2)*100);
            smarticsGaugeToday.updateValues(expected,consumed);
        }
        else
            smarticsGaugeToday.updateValues(0,0);
            //console.log("expected: ", expected);
            //console.log("consumed: ", consumed);
        
    });

}




//------------------------------------------------------------------------------------------------------
//this method will 
function smarticsGetMSEforWeek() {
    //=============================== RMSE & Gauge for smartics NEXT WEEK
    var rcDates=smarticsNextWeekPredictor.getDates();
    //console.log(rcDates);
    var isoStartDate=rcDates.startDate.toISOString().slice(0, 19) + 'Z';
    var isoNowDate=rcDates.nowDate.toISOString().slice(0, 19) + 'Z';

    var url="get_RMSE.php?testbed=SMARTICS" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=1&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		smarticsNextWeekPredictor.setRMSE1(data['1']);
    });
 
    var url="get_RMSE.php?testbed=SMARTICS" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=2&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		smarticsNextWeekPredictor.setRMSE2(data['1']);
        //update gauge
        Consumption=smarticsNextWeekPredictor.getConsumptionIndicators();
            //console.log("total expected." + Consumption.totalExpectedConsumed2);
        if (Consumption.totalExpectedConsumed2 >0) {
            var expected=Math.trunc((Consumption.expectedConsumed2 / Consumption.totalExpectedConsumed2)*100);
            var consumed=Math.trunc((Consumption.totalConsumed / Consumption.totalExpectedConsumed2)*100);
            smarticsGaugeWeek.updateValues(expected,consumed);
        }
        else
            smarticsGaugeWeek.updateValues(0,0);
    });

}






//------------------------------------------------------------------------------------------------------
//this method will get the data form the server for the dashboards
function getDataForSMARTICS(now) {
    smarticsNumberDataStreamsArrived=0
    
    requestURL=buildJSONrequestURL('SMARTICS',"total_power",now,60);
    requestURLPrediction1=buildJSONrequestPredictionURL('SMARTICS',1,now,60);
    requestURLPrediction2=buildJSONrequestPredictionURL('SMARTICS',2,now,60);

    $.getJSON(requestURL, function (data) {
        dashPredictSmartICS.dataArrived(data);
		smarticsNextWeekPredictor.dataArrived(data);
        smarticsTodayPredictor.dataArrived(data);

        smarticsNumberDataStreamsArrived++;
        if (smarticsNumberDataStreamsArrived>=3) {
            smarticsGetMSEforWeek();
            smarticsGetMSEforToday();
        }
     });

    $.getJSON(requestURLPrediction1, function (data) {
        dashPredictSmartICS.dataPrediction1Arrived(data);
		smarticsNextWeekPredictor.dataPrediction1Arrived(data);
        smarticsTodayPredictor.dataPrediction1Arrived(data);

        smarticsNumberDataStreamsArrived++;
        if (smarticsNumberDataStreamsArrived>=3) {
            smarticsGetMSEforWeek();
            smarticsGetMSEforToday();
        }
    });

    $.getJSON(requestURLPrediction2, function (data) {
        dashPredictSmartICS.dataPrediction2Arrived(data);
		smarticsNextWeekPredictor.dataPrediction2Arrived(data);
        smarticsTodayPredictor.dataPrediction2Arrived(data);

        smarticsNumberDataStreamsArrived++;
        if (smarticsNumberDataStreamsArrived>=3) {
            smarticsGetMSEforWeek();
            smarticsGetMSEforToday();
        }
    });


    smarticsChartInternalTemperature.setNowDate(now);
    requestURL=buildJSONrequestURL('SMARTICS',"internal_temperature",now,60);
    $.getJSON(requestURL, function (data) {
        smarticsChartInternalTemperature.dataArrived(data);
    });

    smarticsChartHumidity.setNowDate(now);
    requestURL=buildJSONrequestURL('SMARTICS',"humidity",now,60);
    $.getJSON(requestURL, function (data) {
        smarticsChartHumidity.dataArrived(data);
    });

    smarticsChartHumanActivity.setNowDate(now);
    requestURL=buildJSONrequestURL('SMARTICS',"human_activity",now,60);
    $.getJSON(requestURL, function (data) {
        smarticsChartHumanActivity.dataArrived(data);
    });



	
}


//------------------------------------------------------------------------------------------------------
//this method will 
function realdcGetMSEforToday() {
    //=============================== RMSE & Gauge for REALDC TODAY
    var rcDates=realdcTodayPredictor.getDates();
    //console.log(rcDates);
    var isoStartDate=rcDates.startDate.toISOString().slice(0, 19) + 'Z';
    var isoNowDate=rcDates.nowDate.toISOString().slice(0, 19) + 'Z';

    var url="get_RMSE.php?testbed=REALDC" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=1&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		realdcTodayPredictor.setRMSE1(data['1']);
    });
 
    var url="get_RMSE.php?testbed=REALDC" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=2&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		realdcTodayPredictor.setRMSE2(data['1']);
        //update gauge
        todayConsumption=realdcTodayPredictor.getConsumptionIndicators();
        if (todayConsumption.totalExpectedConsumed2 >0) {
			//console.log("todayConsumption.totalConsumed: " + todayConsumption.totalConsumed);
			//console.log("todayConsumption.expectedConsumed2: " + todayConsumption.expectedConsumed2);
			//console.log("todayConsumption.totalExpectedConsumed2: " + todayConsumption.totalExpectedConsumed2);
			
            var expected=Math.trunc((todayConsumption.expectedConsumed2 / todayConsumption.totalExpectedConsumed2)*100);
            var consumed=Math.trunc((todayConsumption.totalConsumed / todayConsumption.totalExpectedConsumed2)*100);
			//console.log("expected: " + expected);
			//console.log("consumed: " + consumed);
            realdcGaugeToday.updateValues(expected,consumed);
        }
        else
            realdcGaugeToday.updateValues(0,0);
    });

}




//------------------------------------------------------------------------------------------------------
//this method will 
function realdcGetMSEforWeek() {
    //=============================== RMSE & Gauge for REALDC NEXT WEEK
    var rcDates=realdcNextWeekPredictor.getDates();
    //console.log(rcDates);
    var isoStartDate=rcDates.startDate.toISOString().slice(0, 19) + 'Z';
    var isoNowDate=rcDates.nowDate.toISOString().slice(0, 19) + 'Z';

    var url="get_RMSE.php?testbed=REALDC" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=1&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		realdcNextWeekPredictor.setRMSE1(data['1']);
    });
 
    var url="get_RMSE.php?testbed=REALDC" + "&dateBegin=" + isoStartDate + "&dateEnd=" + isoNowDate + "&predictionMethod=2&predictedHours=1" ;
    //console.log(url);
    $.getJSON(url, function (data) {
		realdcNextWeekPredictor.setRMSE2(data['1']);
        //update gauge
        Consumption=realdcNextWeekPredictor.getConsumptionIndicators();
        if (Consumption.totalExpectedConsumed2 >0) {
            //console.log("total expected." + Consumption.totalExpectedConsumed2);
            var expected=Math.trunc((Consumption.expectedConsumed2 / Consumption.totalExpectedConsumed2)*100);
            var consumed=Math.trunc((Consumption.totalConsumed / Consumption.totalExpectedConsumed2)*100);
            realdcGaugeWeek.updateValues(expected,consumed);
        }
        else
            realdcGaugeWeek.updateValues(0,0);
    });

}



//------------------------------------------------------------------------------------------------------
//this method will get the data form the server for the dashboards
function getDataForREALDC(now) {
    realdcNumberDataStreamsArrived=0;
    
    requestURL=buildJSONrequestURL('REALDC',"total_power",now,60);
    requestURLPrediction1=buildJSONrequestPredictionURL('REALDC',1,now,60);
    requestURLPrediction2=buildJSONrequestPredictionURL_v2('REALDC',2,now,60);

    $.getJSON(requestURL, function (data) {
        dashPredictRealDC.dataArrived(data);
        realdcNextWeekPredictor.dataArrived(data);
		realdcTodayPredictor.dataArrived(data);
        
        realdcNumberDataStreamsArrived++;
        if (realdcNumberDataStreamsArrived>=3) {
            realdcGetMSEforWeek();
            realdcGetMSEforToday();
        }
    });

    $.getJSON(requestURLPrediction1, function (data) {
        dashPredictRealDC.dataPrediction1Arrived(data);
        realdcNextWeekPredictor.dataPrediction1Arrived(data);
		realdcTodayPredictor.dataPrediction1Arrived(data);

        realdcNumberDataStreamsArrived++;
        if (realdcNumberDataStreamsArrived>=3) {
            realdcGetMSEforWeek();
            realdcGetMSEforToday();
        }
    });

    
    $.getJSON(requestURLPrediction2, function (data) {
        dashPredictRealDC.dataPrediction2Arrived(data);
        realdcNextWeekPredictor.dataPrediction2Arrived(data);
		realdcTodayPredictor.dataPrediction2Arrived(data);

        realdcNumberDataStreamsArrived++;
        if (realdcNumberDataStreamsArrived>=3) {
            realdcGetMSEforWeek();
            realdcGetMSEforToday();
        }
    });

 
    realdcChartInternalTemperature.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"internal_temperature",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartInternalTemperature.dataArrived(data);
    });

    realdcChartExternalTemperature.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"external_temperature",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartExternalTemperature.dataArrived(data);
    });


    realdcChartHumidity.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"humidity",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartHumidity.dataArrived(data);
    });


    realdcChartRainfall.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"rainfall",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartRainfall.dataArrived(data);
    });


    realdcChartWaterTemperature1.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"water_temperature1",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartWaterTemperature1.dataArrived(data);
    });


    realdcChartWaterTemperature2.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"water_temperature2",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartWaterTemperature2.dataArrived(data);
    });

 
    realdcChartWindSpeed.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"wind_speed",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartWindSpeed.dataArrived(data);
    });

    realdcChartWindChill.setNowDate(now);
    requestURL=buildJSONrequestURL('REALDC',"wind_chill",now,60);
    $.getJSON(requestURL, function (data) {
        realdcChartWindChill.dataArrived(data);
    });
    
}




//------------------------------------------------------------------------------------------------------
//this method will detect dragging in the REALDC energy dashboard and recalculate the date that should be showed
function processDraggingRealdc() {
    $('#energy-iot-dashboard-predict-div').on('mousedown', function(e) {
        //console.log("mousedown");
        $(this).data('isMouseDown', true);
        $(this).data('p0', { x: e.pageX, y: e.pageY });
    })
    .on('mousemove', function(e) {
        if ($(this).data('isMouseDown') == true) {
            //console.log("mousemove");

            var p0 = $(this).data('p0');
            p1 = { x: e.pageX, y: e.pageY },
            horizontalDistance = p1.x - p0.x;
            
            //console.log("Horizontal distance: " + horizontalDistance);
            
            oldDate=dashPredictRealDC.calculateDragDate(0);
            newDate=dashPredictRealDC.calculateDragDate(horizontalDistance);
            //console.log(oldDate);
            //console.log(newDate);
            
            if(newDate.getHours() != oldDate.getHours() ) { //if there was a movement
                $(this).data('p0', { x: e.pageX, y: e.pageY });
                updatedDate=newDate.toISOString().slice(0, 19) + 'Z';
                updateRealdcDashboards(updatedDate); 
            }
                
        }
    })  
    .on('mouseout', function(e) {
        //console.log("mouseout");
        $(this).data('isMouseDown', false);
    })  
    .on('mouseup', function(e) {
        //console.log("mouseup");
        $(this).data('isMouseDown', false);
    })  
} 






//------------------------------------------------------------------------------------------------------
//this method will detect dragging in the SMARTICS energy dashboard and recalculate the date that should be showed
function processDraggingSmartics() {
    $('#energy-iot-dashboard-predict-div-smartics').on('mousedown', function(e) {
        //console.log("mousedown");
        $(this).data('isMouseDown', true);
        $(this).data('p0', { x: e.pageX, y: e.pageY });
    })
    .on('mousemove', function(e) {
        if ($(this).data('isMouseDown') == true) {
            //console.log("mousemove");

            var p0 = $(this).data('p0');
            p1 = { x: e.pageX, y: e.pageY },
            horizontalDistance = p1.x - p0.x;
            
            //console.log("Horizontal distance: " + horizontalDistance);
            
            oldDate=dashPredictSmartICS.calculateDragDate(0);
            newDate=dashPredictSmartICS.calculateDragDate(horizontalDistance);
            //console.log(oldDate);
            //console.log(newDate);
            
            if(newDate.getHours() != oldDate.getHours() ) { //if there was a movement
                $(this).data('p0', { x: e.pageX, y: e.pageY });
                updatedDate=newDate.toISOString().slice(0, 19) + 'Z';
                updateSmarticsDashboards(updatedDate); 
            }
                
        }
    })  
    .on('mouseout', function(e) {
        //console.log("mouseout");
        if ($(this).data('isMouseDown') == true) {
            //console.log("mousemove");

            var p0 = $(this).data('p0');
            p1 = { x: e.pageX, y: e.pageY },
            horizontalDistance = p1.x - p0.x;
            
            //console.log("Horizontal distance: " + horizontalDistance);
            
            var date=dashPredictSmartICS.calculateDragDate(0);
            var updatedDate=date.toISOString().slice(0, 19) + 'Z';
            updateHeatmap(updatedDate); 
                
        }


        $(this).data('isMouseDown', false);
    })  
    .on('mouseup', function(e) {
        if ($(this).data('isMouseDown') == true) {
            //console.log("mousemove");

            var p0 = $(this).data('p0');
            p1 = { x: e.pageX, y: e.pageY },
            horizontalDistance = p1.x - p0.x;
            
            //console.log("Horizontal distance: " + horizontalDistance);
            
            var date=dashPredictSmartICS.calculateDragDate(0);
            var updatedDate=date.toISOString().slice(0, 19) + 'Z';
            updateHeatmap(updatedDate); 
                
        }

    //console.log("mouseup");
        $(this).data('isMouseDown', false);
    })  
} 



//================================================================================================================================
$(document).ready(function () {
    now=getNow();  //the actual date
    
    createObjectsSMARTICS(now);

    //console.log("ready!");
    dashPredictRealDC=new  DashboardPredict("energy-iot-dashboard-predict-div","energy-iot-dashboard-predict-canvas");
    
    realdcTodayPredictor= new nextHoursPredict("realdc-today-div");
    realdcTodayPredictor.setNowDate(now);

	realdcNextWeekPredictor= new nextDaysPredict("realdc-next-week-div");
    realdcNextWeekPredictor.setNowDate(now);
    
    realdcGaugeToday= new energyGauge("realdc-today-gauge-div","realdcGaugeToday","Today");
    realdcGaugeWeek= new energyGauge("realdc-next-week-gauge-div","realdcGaugeWeek","This Week");

    
    realdcChartInternalTemperature= new smallChart("realdc-chart-internal-temperature","fill","Internal air temperature (ºC)");
    realdcChartInternalTemperature.setChartColor('#006633');

    realdcChartExternalTemperature= new smallChart("realdc-chart-external-temperature","fill","External air temperature (ºC)");
    realdcChartExternalTemperature.setChartColor('#006633');

    realdcChartHumidity= new smallChart("realdc-chart-humidity","fill","Relative humidity (%)");
    realdcChartHumidity.setChartColor('#0088cc');

    realdcChartRainfall= new smallChart("realdc-chart-rainfall","bar","Rainfall (l/h)");
    realdcChartRainfall.setChartColor('#0000ff');

    realdcChartWaterTemperature1 = new smallChart("realdc-chart-water-temperature1","fill","Water temperature 1 (ºC)"); 
    realdcChartWaterTemperature1.setChartColor('#007acc');

    realdcChartWaterTemperature2 = new smallChart("realdc-chart-water-temperature2","fill","Water temperature 2 (ºC)"); 
    realdcChartWaterTemperature2.setChartColor('#007acc');

    realdcChartWindSpeed = new smallChart("realdc-chart-wind-speed","fill","Wind speed (Km/h)"); 
    realdcChartWindSpeed.setChartColor('#b35900');

    realdcChartWindChill = new smallChart("realdc-chart-wind-chill","fill","Wind chill (ºC)"); 
    realdcChartWindChill.setChartColor('#8080ff');

    
    getDataForSMARTICS(now);
    getDataForREALDC(now);
    
    updateInterface(now);
    
    processDraggingRealdc()
    processDraggingSmartics();
    
    $( "#REALDC-DIV" ).animate({
        opacity: 1.0}, 1200, function() {
        
        $( "#SMARTICS-DIV" ).animate({
            opacity: 1.0}, 1200, function() {
                
                $("#allbesmart-logo").animate({opacity: 1.0}, 1200);
    
        });

    });
    
        
    $(window).resize(function () {
        //console.log("window resize");
        updateInterface(now);
    });   
});