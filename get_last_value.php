<?php

$testbed=htmlspecialchars($_GET["testbed"]);
$dataSet=htmlspecialchars($_GET["dataSet"]); //this parameter must match the column name of the database
$dataSet=SQLite3::escapeString($dataSet);  //sanitize

if ($testbed=='SMARTICS')
    $db = new SQLite3('/home/eiot/smartics.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='REALDC')
    $db = new SQLite3('/home/eiot/realdc.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='ADREAM')
    $db = new SQLite3('/home/eiot/adream.sqlite3',SQLITE3_OPEN_READONLY);
else
    die("Unknown testbed: ".$testbed);

//echo "testbed:".$testbed;
//echo "dataSet:".$dataSet;
//echo "<br>";


$stmt = $db->prepare("SELECT MAX(datetime) AS datetime,".$dataSet." FROM observation WHERE ".$dataSet." NOT NULL;");

$results = $stmt->execute();
$row = $results->fetchArray();

echo ("{");
echo("\"datetime\":");
echo("\"".$row['datetime']."\",");
echo("\"value\":");
echo("\"".$row[$dataSet]."\"");
echo ("}");
    
?>