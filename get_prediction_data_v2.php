<?php

$testbed=htmlspecialchars($_GET["testbed"]);
$datetimeBegin=htmlspecialchars($_GET["dateBegin"]);
$datetimeEnd=htmlspecialchars($_GET["dateEnd"]);
$methodID=htmlspecialchars($_GET["methodID"]);

//echo "opening database...";
//echo "opening database...";
if ($testbed=='SMARTICS')
    $db = new SQLite3('/home/eiot/smartics.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='REALDC')
    $db = new SQLite3('/home/eiot/realdc.sqlite3',SQLITE3_OPEN_READONLY);
elseif ($testbed=='ADREAM')
    $db = new SQLite3('/home/eiot/adream.sqlite3',SQLITE3_OPEN_READONLY);
else
    die("Unknown testbed: ".$testbed);

//echo $datetimeBegin;
//echo "<br>";
//echo $datetimeEnd;

$_HOUR_OFFSET=5;

$predictionStartDate = new DateTime($datetimeBegin);
for ($i=0; $i<$_HOUR_OFFSET; $i++) //go back 'n' hours to the time when the prediction has been made
    $predictionStartDate->sub(new DateInterval('PT01H'));

$predictionEndDate = new DateTime($datetimeEnd);
for ($i=0; $i<$_HOUR_OFFSET; $i++) //go back 'n' hours to the time when the prediction has been made
    $predictionEndDate->sub(new DateInterval('PT01H'));

$ISOpredictionStartDate = $predictionStartDate->format('Y-m-d')."T".$predictionStartDate->format('H').":00:00Z";
$ISOpredictionEndDate = $predictionEndDate->format('Y-m-d')."T".$predictionEndDate->format('H').":00:00Z";

//echo "<br>.$ISOpredictionStartDate";
//echo "<br>.$ISOpredictionEndDate";
//echo "<br>";


$stmt = $db->prepare("SELECT * FROM prediction WHERE hour_offset=? AND datetime>=? AND datetime<=? AND predition_method_id=? ORDER BY datetime;");
//$stmt = $db->prepare("SELECT * FROM prediction WHERE hour_offset=1;");
$stmt->bindValue(1,$_HOUR_OFFSET,SQLITE3_INTEGER);
$stmt->bindValue(2,$ISOpredictionStartDate,SQLITE3_TEXT);
$stmt->bindValue(3,$ISOpredictionEndDate,SQLITE3_TEXT);
$stmt->bindValue(4,$methodID,SQLITE3_TEXT);

$results = $stmt->execute();

echo ("{");
$firstData=true;

while ($row = $results->fetchArray()) {
    //check if it is a number
    $predValue=$row['predicted_value'];
    if (!is_int($predValue) && !is_float($predValue))
        continue;
    
    if ($firstData == false)
        echo (",");
    else
        $firstData=false;
    
    echo("\"");
    $realDate = new DateTime($row['datetime']);

    for ($i=0; $i<$_HOUR_OFFSET; $i++) //go forward 'n' hours to the time the prediction refers to
        $realDate->add(new DateInterval('PT01H'));

    echo $realDate->format('Y-m-d')."T".$realDate->format('H').":00:00Z";
    echo("\": ");

    echo("\"");
    echo($predValue);
    echo("\"");
}


//Now, the predictions for the next hours, taken from the last date sent by the requester
$stmt = $db->prepare("SELECT * FROM prediction WHERE datetime=? AND predition_method_id=? ORDER BY hour_offset;");
$stmt->bindValue(1,$datetimeEnd,SQLITE3_TEXT);
$stmt->bindValue(2,$methodID,SQLITE3_TEXT);

$results = $stmt->execute();

while ($row = $results->fetchArray()) {
    $predValue=$row['predicted_value'];
    if (!is_int($predValue) && !is_float($predValue))
        continue;

    if ($firstData == false)
        echo (",");
    else
        $firstData=false;
    
    echo("\"");
    $realDate = new DateTime($row['datetime']);
    
    //add hour_offset
    for($i=0; $i<$row['hour_offset']; $i++) 
        $realDate->add(new DateInterval('PT01H'));
    
    echo $realDate->format('Y-m-d')."T".$realDate->format('H').":00:00Z";
    echo("\": ");

    echo("\"");
    echo($predValue);
    echo("\"");
}




echo ("}");
    
?>