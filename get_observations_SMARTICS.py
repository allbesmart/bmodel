#This script verifies if there are new observations in the fiesta registry for a specific testbed
#Testbeds are idenfified by GPS coordinates
#New observations are inserted into the local database

import requests
import fiesta_auth
import json
import sqlite3 as lite
import sys
import logging
import datetime
import time

con = None
cur = None
token= None
logging.basicConfig(filename='get_observations_SMARTICS.log',format='%(asctime)s %(message)s',level=logging.DEBUG)

databasePath="/home/eiot/energy-iot.sqlite3"

#things that have to be changed for each testbed
testbed_id=1 #smart-ics
testbed_latitude=51.2433445
    
sparqlQuery="Prefix ssn: <http://purl.oclc.org/NET/ssnx/ssn#>\n\
Prefix iotlite: <http://purl.oclc.org/NET/UNIS/fiware/iot-lite#>\n\
Prefix dul: <http://www.loa.istc.cnr.it/ontologies/DUL.owl#>\n\
Prefix geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n\
Prefix time: <http://www.w3.org/2006/time#>\n\
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n\
Prefix m3-lite: <http://purl.org/iot/vocab/m3-lite#>\n\
Prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n\
select ?sensorID ?datetime ?value ?latitude ?longitude ?qk ?unit\n\
where {\n\
    ?o a ssn:Observation.\n\
    ?o ssn:observedBy ?sensorID.\n\
    ?o ssn:observedProperty/rdf:type ?qk.\n\
    VALUES ?qk {m3-lite:Temperature m3-lite:RoomTemperature m3-lite:Power m3-lite:Humidity m3-lite:Distance }\n\
    ?o ssn:observationSamplingTime ?t.\n\
    ?o geo:location ?point.\n\
    ?point geo:lat ?latitude.\n\
    VALUES ?latitude {5.12433445E1}\n\
    ?point geo:long ?longitude.\n\
    ?t time:inXSDDateTime ?datetime.\n\
    ?o ssn:observationResult ?or.\n\
    ?or  ssn:hasValue ?v.\n\
    ?v iotlite:hasUnit/rdf:type ?unit.\n\
    ?v dul:hasDataValue ?value.\n\
} order by ?datetime"




#=============================================================================================================
#this function inserts a new sensor in the database
def insertSensor(testbed_id,obs_id_sensor,obs_qk,obs_unit):
    #print "Inserting sensor " + obs_id_sensor
    #insert...
    cur.execute("INSERT INTO sensor (testbed_id,fiesta_id_sensor,qk,unit) VALUES (?,?,?,?);",(testbed_id,obs_id_sensor,obs_qk,obs_unit,))
 
    #now, get its id
    cur.execute("SELECT id_sensor FROM sensor WHERE fiesta_id_sensor=?;",(obs_id_sensor,))
    queryResult = cur.fetchone()
    
    if (queryResult != None and queryResult[0] != None):
        databaseSensorId=queryResult[0];
        return databaseSensorId
    else:
        print "What?????"
        sys.exit(1)


#=============================================================================================================
#this function checks if a sensor exists in the database and returns its database id
#otherwise reads sensor's data from fiesta and inserts a new record in the database
def checkSensor(testbed_id,obs_id_sensor,obs_qk,obs_unit):
    #print "Checking sensor " + obs_id_sensor
    cur.execute("SELECT id_sensor FROM sensor WHERE fiesta_id_sensor=?;",(obs_id_sensor,))
    queryResult = cur.fetchone()
    
    if (queryResult != None and queryResult[0] != None):
        databaseSensorId=queryResult[0];
        #print "This sensor already exists in the database, id=" + str(databaseSensorId)
    else:
        #print "This sensor doesn't exist in the database. Adding..."
        databaseSensorId=insertSensor(testbed_id,obs_id_sensor,obs_qk,obs_unit)

    #temp=raw_input("id=" + str(databaseSensorId) +" ,press enter");
    return databaseSensorId


    
#=============================================================================================================
#this function checks if an observation already exists in the database
def observationAlreadyExists(param_id_sensor,param_datetime):    
    cur.execute("SELECT id_sensor FROM observation WHERE id_sensor=? AND datetime=?;",(param_id_sensor,param_datetime))
    queryResult = cur.fetchone()
    
    if (queryResult != None and queryResult[0] != None):
        return 1
    else:
        return False


        
    
#=============================================================================================================
#this function will get observations from JSON results and then
#put it in the local database
def processResults(testbed_id, jsonResults ):
    #print "Processing results...testbed_id:" + testbed_id
    numberExistingObservations = 0
    numberNewObservations = 0

    for c in jsonResults['items']:
        temp=c.get('qk')
        #detetar estrutura JSON vazia...
        if (temp==None):
            print("No results...")
            continue
    
        #print('---------------------------------------------------')

        obs_id_sensor=c.get('sensorID')
        #print(' ')
        #print ('fiesta_id_sensor:'+ obs_id_sensor)

        temp=c.get('datetime')
        obs_datetime=temp.split('^^')[0]
        #print(' ')
        #print ('datetime:'+ obs_datetime)

        temp=c.get('value')
        value=temp.split('^^')[0]
        obs_value=float(value)
        #print(' ')
        #print ('value:' + str(obs_value))

        temp=c.get('latitude')
        lat=temp.split('^^')[0]
        obs_latitude=float(lat)
        #print(' ')
        #print ('latitude:' + str(obs_latitude))
       
        temp=c.get('longitude');
        long=temp.split('^^')[0]
        obs_longitude=float(long)
        #print(' ')
        #print ('longitude:' + str(obs_longitude))

        temp=c.get('qk')
        obs_qk=temp.split('#')[1]
        #print(' ')
        #print ('qk:' + obs_qk)

        temp=c.get('unit')
        obs_unit=temp.split('#')[1]
        #print(' ')
        #print ('unit:' + obs_unit)
        
        #check if sensor already exists in database, else put it there
        database_sensor_id=checkSensor(testbed_id,obs_id_sensor,obs_qk,obs_unit)
        
        #now, check if this observation already exists in the database
        if observationAlreadyExists(database_sensor_id,obs_datetime):
            #print "This Observation already exists..."
            numberExistingObservations +=1
        else:
            #print "This is a new observation... inserting it"
            cur.execute("INSERT INTO observation (datetime,id_sensor,lat,long,qk,unit,value) VALUES (?,?,?,?,?,?,?);",(obs_datetime,database_sensor_id,obs_latitude,obs_longitude,obs_qk,obs_unit,obs_value,))
            numberNewObservations +=1

    con.commit()
    msg = str(numberNewObservations) + " new observations, " + str(numberExistingObservations) + " observations already exist in the database"
    print msg
    logging.debug(msg)



#============================== MAIN PROGRAM ====================================

numberArguments=len(sys.argv)
#print 'Number of arguments:', numberArguments, 'arguments.'
#print 'Argument List:', str(sys.argv)    
    
if numberArguments>2:
    print 'usage: python get_observations_SMARTICS.py [yyyy-mm-ddThh:mm:ssZ]'
    sys.exit(1)
    
if numberArguments>1:
    initialTime=sys.argv[1]
else:
    initialTime=None
    
try:
    con=lite.connect(databasePath)
    cur=con.cursor()
        

    msg="=============================== Starting Script ==================================";
    print msg
    logging.debug(msg)
     
    if initialTime == None:
        #get the date of the most recent observation from this testbed
        cur.execute("SELECT MAX(datetime) FROM observation WHERE (lat=?)",(testbed_latitude,))
        res = cur.fetchone()

        if (res[0]==None):
            lastDate="2017-11-01T00:00:00Z"
        else:
            lastDate=res[0]
    else:
        lastDate=initialTime

    msg="Getting new observations since " + lastDate
    print msg
    logging.debug(msg)
    
    dateTimeBegin = datetime.datetime.strptime(lastDate,"%Y-%m-%dT%H:%M:%SZ")
    #set to the nearest hour
    dateTimeBegin=dateTimeBegin.replace(minute=0,second=0)
    
    #get time slices with this duration
    timeSlice=datetime.timedelta(minutes=60)
    #the next time slice will advance by timeAdvance
    timeAdvance=datetime.timedelta(minutes=60)   
    
    dateTimeEnd=dateTimeBegin + timeSlice

    dateTimeFinal= datetime.datetime.now() + datetime.timedelta(hours=1)
    
    #time to wait when the server sends an error
    waitingTime=10  #10 seconds

    while (dateTimeEnd < dateTimeFinal):
        msg="retrieving time slice from " + str(dateTimeBegin) + " to " + str(dateTimeEnd)
        print msg
        logging.debug(msg)
        
        #login na plataforma
        token=fiesta_auth.login()
        if token == None:
            msg= "Login failure"
            print msg
            logging.debug(msg)
            sys.exit(1)
    
        urlDateBegin=dateTimeBegin.strftime("%Y%m%d%H%M")
        urlDateEnd=dateTimeEnd.strftime("%Y%m%d%H%M")
        
        url="https://platform.fiesta-iot.eu/iot-registry/api/queries/execute/observations?from=" + urlDateBegin + "&to=" + urlDateEnd
        #print url;
        #sys.exit(1)
       
        headers={'iPlanetDirectoryPro':token,'Content-Type':'text/plain','Accept':'application/json'}
        
        r = requests.post(url,headers=headers,data=sparqlQuery,timeout=300)
        
        #logout
        fiesta_auth.logout(token)
        token=None

        #print r.status_code
        #print r
   
        if (r.status_code!=200):
            msg= "Error, status code=" + str(r.status_code)
            print msg
            logging.debug(msg)
            if (waitingTime < 300):
                msg= "Waiting " + str(waitingTime) + " seconds to retry same query"
                print msg
                logging.debug(msg)

                time.sleep(waitingTime)
                #exponential waiting time
                waitingTime = waitingTime * 2
                continue
            else:
                msg= "Request failed, advancing to next time slot"
                print msg
                logging.debug(msg)
        else: #if status_code==200, process results
            parsed=r.json()  
            #print parsed;
            processResults(testbed_id,parsed)
            #a=raw_input("Press Enter to continue...")
             
        waitingTime=10  #reset the waitingTime
        #advance to the next time slice
        dateTimeBegin=dateTimeBegin + timeAdvance
        dateTimeEnd=dateTimeEnd + timeAdvance
        
        #wait 1 minute between requests - to avoid server overload
        msg="waiting 30s until next request..."
        print msg
        logging.debug(msg)
        time.sleep(30)
        
    
    #close db
    con.close()
    con= None

    msg="======== Database closed, gracefully terminating...."
    print msg
    logging.debug(msg)
        

except lite.Error, e:
    print ('Error %s:' % e.args[0])
    if token!= None:
        fiesta_auth.logout(token)
    sys.exit(1)
    
except requests.exceptions.RequestException as e:
    print e
    logging.debug(e)
    sys.exit(1)

except requests.exceptions.Timeout as e:
    print e
    logging.debug(e)
    sys.exit(1)

finally:
    if con:
        con.commit()
        con.close()
    if token!=None:
        fiesta_auth.logout(token)
