import fiesta_auth
import json
import sqlite3 as lite
import sys

#token=fiesta_auth.login()
#fiesta_auth.logout(token)

con = None

with open('sensors.txt', 'r') as handle:
    parsed = json.load(handle)

try:
    con=lite.connect('energy-iot.sqlite3')
    cur=con.cursor()

    for c in parsed['items']:
        print('---------------------------------------------------')

        sensor_id=c.get('sensor')
        print(' ')
        print ('sensor_id:'+ sensor_id)

        temp=c.get('endp')
        endpoint=temp.split('^^')[0]
        print(' ')
        print ('endpoint:' + endpoint)

        temp=c.get('type')
        type=temp.split('#')[1]
        print(' ')
        print ('type:' + type)

        temp=c.get('qk')
        qk=temp.split('#')[1]
        print(' ')
        print ('qk:' + qk)

        temp=c.get('unit')
        unit=temp.split('#')[1]
        print(' ')
        print ('unit:'+unit)

        temp=c.get('long');
        long=temp.split('^^')[0]
        long=float(long)
        print(' ')
        print ('long:'+ str(long))

        temp=c.get('lat')
        lat=temp.split('^^')[0]
        lat=float(lat)
        print(' ')
        print ('lat:'+ str(lat))

        sql="INSERT or REPLACE INTO sensor (id_sensor, endpoint,type,qk,lat,long,unit)"
        sql=sql + " VALUES ('" + sensor_id +"','" + endpoint + "','" + type + "','" + qk + "'," + str(lat) +"," + str(long) +",'" + unit +"');"
        print (sql)

        cur.execute(sql)


except lite.Error, e:
    print ('Error %s:' % e.args[0])
    sys.exit(1)

finally:
    if con:
        con.commit()
        con.close()

