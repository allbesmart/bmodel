function DashboardPredict(parent_div_id,canvas_id) {
    //console.log("parent_div_id: " + parent_div_id + " ; canvas_id=" + canvas_id)
    this.jQueryParentDiv=$("#"+parent_div_id);
    this.jQueryCanvas=$("#"+canvas_id);
    this.canvasElement = document.getElementById(canvas_id);
    this.ctx = this.canvasElement.getContext('2d');
    
    
    this.nowDate= new Date(); //this object will contain the most recent date to be shown in the power chart
    this.endDate= new Date(); //this object will contain the topmost date to be shown in the predict chart
    this.startDate= new Date(); //this object will contain the oldest date to be shown in the power and predict charts

    this.chartData=null; //will contain power consumption data
    this.chartPrediction1=null; //will contain prediction data
    this.chartPrediction2=null; //will contain prediction data
    
    this.verticalScale = {
        verticalLineValue:5,  //the value to appear next to the first vertical guideline
        pixelsPerWatt:1,  //scale to be used when drawing the chart lines
        powerLabel:"W" //label to be used next to the verticalLineValue
    };
 
 
    //design properties are stored in this.dP
    this.dP = {
        top_margin:10,
        bottom_margin:40,
        left_margin:40,
        right_margin:8,
        number_sec_lines:5,
        number_predicted_hours:0,
        pixels_per_hour:2,
        tickerSize:5,
        chartColor: '#009900',
        chartStartGradientColor: '#4dff4d',
        backgroundColor: '#ffebe6',
        backLinesColor: '#303030',
        backLinesColor2: '#CACACA',
        predictionLabel: '',
        prediction1Color: '#cc3359 ',
        prediction2Color: '#0052cc',
        powerTitle: 'Power consumption',
        prediction1Title: 'Univariate prediction model',
        prediction2Title: 'Multivariate prediction model'
    };
    
    //=================================================================================
    //this method sets the date that will be the most recent date to be shown in the chart
    this.setNowDate = function(dateString) {
        this.nowDate=new Date(dateString);
        this.nowDate.setMinutes(0);
        this.nowDate.setSeconds(0);
        //console.log(nowDate);
    }


    //=================================================================================
    //this method will recalculate the chart and draw it
    this.update = function() {
        //console.log("update Interface");
        div_width=this.jQueryParentDiv.width();
        div_height=this.jQueryParentDiv.height();
        //console.log("Largura: " + div_width + " , altura: " + div_height);
    
        this.jQueryCanvas.css({top: 4, left: 4, position:'absolute'});
        //$("#energy-iot-dashboard-predict-canvas").width(div_width-10);
        //$("#energy-iot-dashboard-predict-canvas").height(div_height-10);
   
        this.ctx.canvas.width = div_width-10;
        this.ctx.canvas.height = div_height-10;
         
        //draw chart lines
        this.drawBackLines();
        
        //if there's data to be drawn...
        if (this.chartData && typeof(this.chartData) !== "undefined") {
            //console.log("drawing...");
            this.calculateChartDates();
            minMaxPower=this.calculateMinMaxPower(this.chartData,this.chartPrediction1,this.chartPrediction2);
            //console.log("max power: " + maxPower);

            this.verticalScale= this.setupScale(minMaxPower);
            
            //console.log(this.verticalScale);
            
            this.drawVerticalLabels();
            this.drawDateLabels();

            this.drawChartFill();
            this.drawPowerLine(this.chartData,this.dP.chartColor);

            this.drawPowerLine(this.chartPrediction1,this.dP.prediction1Color,true);
            //this.drawCircles(this.chartPrediction1,this.dP.prediction1Color,true);
            this.drawPowerLine(this.chartPrediction2,this.dP.prediction2Color,true);
            //this.drawCircles(this.chartPrediction2,this.dP.prediction2Color,true);
        }
        
        this.drawLegends();
    };

    //=================================================================================================
    //this method returns the horizontal position where the main chart begins
    this.getChartAreaLocation = function() {
        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;
        
        //the square that delimits the prediction zone
        var predictionWidth=this.dP.pixels_per_hour*this.dP.number_predicted_hours;
        var mainChartWidth=graphArea_right-graphArea_left-predictionWidth;
        
        return {
            top: graphArea_top,
            left: graphArea_left,
            bottom: graphArea_bottom,
            right: graphArea_right-predictionWidth,
            height: graphArea_bottom-graphArea_top,
            width: mainChartWidth
        };
    };



    
    
    
   //=================================================================================
    //this method draws the date labels
    this.drawDateLabels = function() {
        //console.log("drawVerticalLines, this.dP.horizontalTicks:" + this.dP.horizontalTicks);

        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;
        
        //console.log(this.nowDate);
        var datePointer=new Date(this.endDate);
        //console.log(datePointer);
        var xPosition=graphArea_right;
        
        while (datePointer > this.startDate ) {
            
            //if it is an hour tick, draw it
            if (datePointer.getHours() == 0) {
                //console.log("Hour tick");
                //console.log(datePointer);
                this.ctx.beginPath();
                this.ctx.lineWidth = 0.2;
                this.ctx.strokeStyle = '#000000';
                this.ctx.setLineDash([2,2]);


                this.ctx.moveTo(xPosition,graphArea_top)
                this.ctx.lineTo(xPosition,graphArea_bottom + 15);
                this.ctx.stroke();
            }
            
            //if it is noon, write the date
            if (datePointer.getHours() == 12) {
                //console.log("noon");
                //console.log(datePointer);

                this.ctx.font = '7pt Verdana';
                this.ctx.textAlign = 'center';
                this.ctx.fillStyle = this.dP.backLinesColor;
                this.ctx.textBaseline = 'middle';

                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var textualDate= monthNames[datePointer.getMonth()];
                textY=graphArea_bottom + 8 ;
                this.ctx.fillText(textualDate,xPosition,textY);

                var textualDate= datePointer.getDate();
                textY=graphArea_bottom + 18 ;
                this.ctx.fillText(textualDate,xPosition,textY);
            }

            var oldDate= new Date(datePointer);
            datePointer.setHours(datePointer.getHours() -1);
            if (datePointer.getTime() == oldDate.getTime() ) {  //summer time change
                datePointer.setHours(datePointer.getHours() - 2);
                xPosition -= this.dP.pixels_per_hour;
            }
            
            
            xPosition -= this.dP.pixels_per_hour;
        }
    }
     


     
     


    //=================================================================================================
    //this method draws the chart's legends
    this.drawLegends = function() {
        res=this.getChartAreaLocation();
  
        //    top: graphArea_top,
        //    left: graphArea_right-rectWidth,
        //    bottom: graphArea_bottom,
        //    right: graphArea_right,
        //    height: rectHeight,
        //    width: rectWidth

        this.ctx.font = '7pt Verdana';
        this.ctx.textAlign = 'left';
        this.ctx.fillStyle = 'black';
        this.ctx.textBaseline = 'left';
        this.ctx.setLineDash([]); //removedash   
        this.ctx.lineWidth = 3;
        
        var lineLength=18;
        var xPos=res.left;
        var yPos=this.ctx.canvas.height -5;
        
        //Power consumption
        this.ctx.beginPath();

        this.ctx.strokeStyle = this.dP.chartColor;
        this.ctx.moveTo(xPos,yPos);
        this.ctx.lineTo(xPos + lineLength,yPos);
        this.ctx.stroke(); 

        this.ctx.fillText(this.dP.powerTitle,xPos + lineLength + 3,yPos);        

        //Prediction method 1
        xPos=xPos + this.ctx.measureText(this.dP.powerTitle).width + 50;
        this.ctx.beginPath();
        this.ctx.strokeStyle = this.dP.prediction1Color;
        this.ctx.moveTo(xPos,yPos);
        this.ctx.lineTo(xPos + lineLength,yPos);
        this.ctx.stroke(); 
        
        this.ctx.fillText(this.dP.prediction1Title,xPos + lineLength + 3,yPos);        

        //Prediction method 2
        xPos=xPos + this.ctx.measureText(this.dP.prediction1Title).width + 50;
        this.ctx.beginPath();
        this.ctx.strokeStyle = this.dP.prediction2Color;
        this.ctx.moveTo(xPos,yPos);
        this.ctx.lineTo(xPos + lineLength,yPos);
        this.ctx.stroke(); 
        
        this.ctx.fillText(this.dP.prediction2Title,xPos + lineLength + 3,yPos);        

    }
     
 
    //=================================================================================================
    //this method draws the back lines of the dashboard and labels
    this.drawBackLines = function() {
        //some calculations
        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;
        graphArea_width=graphArea_right-graphArea_left;
        graphArea_height=graphArea_bottom-graphArea_top;
        

        var my_gradient = this.ctx.createLinearGradient(graphArea_left, graphArea_top, graphArea_left, graphArea_bottom);
        my_gradient.addColorStop(0, this.dP.backgroundColor);
        my_gradient.addColorStop(1, 'white');
        this.ctx.fillStyle = my_gradient;
        this.ctx.fillRect(graphArea_left,graphArea_top,graphArea_width,graphArea_height);
        
        //main lines
        this.ctx.lineWidth = 0.9;
        this.ctx.strokeStyle = this.dP.backLinesColor;
        this.ctx.rect(graphArea_left,graphArea_top,graphArea_width,graphArea_height);
        this.ctx.stroke(); 
        
        //secondary lines
        var secLineVerticalSpacing=Math.trunc((graphArea_bottom-graphArea_top)/this.dP.number_sec_lines);
        //console.log("Vertical spacing of secundary lines: " + secLineVerticalSpacing);
        
        //style for secondary lines
        this.ctx.lineWidth = 1;
        this.ctx.setLineDash([2,2]);
        this.ctx.strokeStyle = this.dP.backLinesColor2;

        //now, draw the secondary lines
        var secLineVerticalPosition= graphArea_bottom - secLineVerticalSpacing + 0.5; //0.5 trick to avoid antialiasing
        while (secLineVerticalPosition > graphArea_top) {
            this.ctx.beginPath();
            this.ctx.moveTo(graphArea_left+2,secLineVerticalPosition);
            this.ctx.lineTo(graphArea_right,secLineVerticalPosition);
            this.ctx.closePath();
            this.ctx.stroke();
            
            secLineVerticalPosition -= secLineVerticalSpacing;
        }
        
      

    };    
 
    //=================================================================================================
    //this method draws the vertical scale labels
    this.drawVerticalLabels = function() {
        if (!this.verticalScale || typeof(this.verticalScale) == "undefined")
            return;
        
        //some calculations
        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        secLineVerticalSpacing=Math.trunc((graphArea_bottom-graphArea_top)/this.dP.number_sec_lines);

        //style for text labels
        this.ctx.font = '6pt Verdana';
        this.ctx.textAlign = 'right';
        this.ctx.fillStyle = this.dP.backLinesColor;
        this.ctx.textBaseline = 'middle';


        var secLineVerticalPosition= graphArea_bottom + 0.5; //0.5 trick to avoid antialiasing
        secLineValue=this.verticalScale.minLabelValue;
        while (secLineVerticalPosition > graphArea_top) {
            this.ctx.fillText(''+secLineValue + ' ' + this.verticalScale.powerLabel, graphArea_left-5, secLineVerticalPosition);
            secLineValue+=this.verticalScale.verticalInterval;
            
            secLineVerticalPosition -= secLineVerticalSpacing;
        }

    }
 
    //=================================================================================================
    //this method calculates the vertical scale and scale labels
    this.setupScale = function(minMaxValues) {
        scaleBaseValues = [4,5,10,20,25];
        minBaseValues = [1,2,4,5,10,];

        multiplier=1;
        maxSupportedValue=0;
        secLineVerticalSpacing=Math.trunc((graphArea_bottom-graphArea_top)/this.dP.number_sec_lines);
        secVerticalSpacing = secLineVerticalSpacing * this.dP.number_sec_lines;
        
        maxValue=minMaxValues.maxPower;
        minValue=minMaxValues.minPower;
        
        //console.log("minValue:"+ minValue + "  ,maxValue:" + maxValue);
        
        if (maxValue==0)
            return  {"verticalLineValue": 10,
                    "powerLabel":"W",
                    "pixelsPerWatt": 0,
                    "minSupportedValue": 0,
                    "maxSupportedValue": 0,
					"minLabelValue": 0,
                    "verticalInterval": 0                   
                    };

        while (maxSupportedValue < maxValue) {
            for (x in scaleBaseValues) {
                scaleBaseValue=scaleBaseValues[x];
                maxSupportedValue=multiplier * scaleBaseValue * this.dP.number_sec_lines;
                //console.log("max supportedValue = " + maxSupportedValue);
                if (maxSupportedValue >= maxValue) { //found a solution
                    verticalLineValue= multiplier * scaleBaseValue;
                    
                    for (z in minBaseValues) {
                        minBaseValue=minBaseValues[z];
                        minSupportedValue = maxSupportedValue - multiplier * minBaseValue * this.dP.number_sec_lines;
                        if (minSupportedValue < minValue )
                            break;
                    }
                    
                    //console.log("minSupportedValue: " + minSupportedValue);
                    //console.log("maxSupportedValue: " + maxSupportedValue);
                    //console.log("verticalLineValue: " + verticalLineValue);
                    
                    
                    if (verticalLineValue < 1000)
                        return {"verticalLineValue":verticalLineValue,
                                "powerLabel":"W",
                                "pixelsPerWatt": secVerticalSpacing / (maxSupportedValue-minSupportedValue),
                                "minSupportedValue": minSupportedValue,
                                "maxSupportedValue": maxSupportedValue,
								"minLabelValue": minSupportedValue / 1000000,
                                "verticalInterval": minBaseValue * multiplier
                                };
                    
                    else if (verticalLineValue < 1000000)
                        return {"verticalLineValue":verticalLineValue/1000,
                                "powerLabel":"KW",
                                "pixelsPerWatt": secVerticalSpacing / (maxSupportedValue-minSupportedValue),
                                "minSupportedValue": minSupportedValue,
                                "maxSupportedValue": maxSupportedValue,
								"minLabelValue": minSupportedValue / 1000000,
                                "verticalInterval": (minBaseValue * multiplier) / 1000
                                };
                    
                    else return {"verticalLineValue":verticalLineValue/1000000,
                                "powerLabel":"MW",
                                "pixelsPerWatt": secVerticalSpacing / (maxSupportedValue-minSupportedValue),
                                "minSupportedValue": minSupportedValue,
                                "maxSupportedValue": maxSupportedValue,
								"minLabelValue": minSupportedValue / 1000000,
                                "verticalInterval": (minBaseValue * multiplier) /1000000
                                };
                }
            }
         multiplier=multiplier * 10;
        }
    };


    
    //=================================================================================================
    //this method calculates the start, now and end dates for the chart, assuming its current dimension
    this.calculateChartDates = function() {
        //some calculations
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;

        //console.log("this.nowDate: " + this.nowDate);

        this.endDate = new Date(this.nowDate);
        this.endDate.setHours(this.endDate.getHours() + this.dP.number_predicted_hours);
        //console.log("this.endDate: " + this.endDate);

        totalChartHours= Math.trunc((graphArea_right-graphArea_left) / this.dP.pixels_per_hour);
        //console.log("This chart supports " + totalChartHours + " hours");
        
        this.startDate  = new Date(this.endDate);
        this.startDate.setHours(this.startDate.getHours() - totalChartHours - 1);
        //console.log("this.startDate: " + this.startDate);
    };

 
   //=================================================================================================
    //this method calculates the minimum and maximum values in both measured and estimated power
    this.calculateMinMaxPower = function(dataValues,pred1Values,pred2Values) {
        //some calculations
        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;
        
        //console.log(dataValues);

        var maximumPower=0;
        var minimumPower=Number.MAX_VALUE;
        
        //console.log("Calculating max power from " + this.startDate + " to " + this.nowDate);
        //inspect data from dateStart to dateEnd and find maximum values
        var datePointer= new Date(this.startDate);
        //console.log("datePointer: " + datePointer);
        
        while (datePointer.getTime() <= this.endDate.getTime()) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            
            if (dataValues && typeof(dataValues[isoDate]) !== "undefined") {
                reportedValue=dataValues[isoDate] * 1.0;  //assure that it's a number
                //console.log("reportedValue:" + reportedValue);
                if (reportedValue && reportedValue > maximumPower)
                    maximumPower=reportedValue;

                if (reportedValue && reportedValue < minimumPower)
                    minimumPower=reportedValue;
            }

            if (pred1Values && typeof(pred1Values[isoDate]) !== "undefined") {
                reportedValue=pred1Values[isoDate] * 1.0;  //assure that it's a number
                //console.log("reportedValue:" + reportedValue);

                if (reportedValue && reportedValue > maximumPower)
                    maximumPower=reportedValue;

                if (reportedValue && reportedValue < minimumPower)
                    minimumPower=reportedValue;
            }
                        

            if (pred2Values && typeof(pred2Values[isoDate]) !== "undefined") {
                reportedValue=pred2Values[isoDate] * 1.0;  //assure that it's a number
                //console.log("reportedValue:" + reportedValue);

                if (reportedValue && reportedValue > maximumPower)
                    maximumPower=reportedValue;

                if (reportedValue && reportedValue < minimumPower)
                    minimumPower=reportedValue;
            }
                                    
            datePointer.setHours(datePointer.getHours()+1);
            
        }
        
        //console.log("maximumPower: " + maximumPower);
        //console.log("minimumPower: " + minimumPower);
        
        return {maxPower: maximumPower, minPower: minimumPower };
    };





    
 
 //=================================================================================================
    //this method draws a power line
    this.drawPowerLine = function(dataSource,lineColor,isPrediction) {
        //some calculations
        if (!dataSource)
            return;
        
        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;

        //draw from dateNow to dateStart
        var datePointer= new Date(this.nowDate);
        //console.log("datePointer: " + datePointer);
        if (isPrediction)
            datePointer.setHours(datePointer.getHours() + this.dP.number_predicted_hours);

        //line style
        this.ctx.setLineDash([]); //remove dash
        this.ctx.strokeStyle = lineColor;
        this.ctx.lineWidth = 2;
        
        var firstPointHorizontalPosition;
        
        var firstPoint=1;
        var drawingNoDataZone=0;

        var pointVerticalPosition,lastPointHorizontalPosition,lastPointVerticalPosition;

        if (isPrediction) //if this a prediction line, start on the full right side
            pointHorizontalPosition = graphArea_right;
        else
            pointHorizontalPosition = graphArea_right - this.dP.number_predicted_hours * this.dP.pixels_per_hour ;
        
        firstPointHorizontalPosition=pointHorizontalPosition;
        lastPointHorizontalPosition=pointHorizontalPosition;
        lastPointVerticalPosition=graphArea_bottom;
 
        this.ctx.beginPath();
      
        while ( datePointer.getTime() >= this.startDate.getTime() ) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            
            if (typeof(dataSource[isoDate]) == "undefined" || !dataSource[isoDate]) {
                if (!drawingNoDataZone) {//if its a transition between a data and "no data" zone
                    this.ctx.stroke();
                    this.ctx.beginPath();
                    this.ctx.setLineDash([3,3]); //create a dash
                    this.ctx.lineWidth = 1;
                    if (!firstPoint)
                        this.ctx.moveTo(lastPointHorizontalPosition,lastPointVerticalPosition);
                }
                drawingNoDataZone=1;                   
            }
            else { //else, if there is data
                reportedValue=dataSource[isoDate] * 1.0;
                //console.log("reportedValue:" + reportedValue + " , screenHeight: " + reportedValue * this.verticalScale.pixelsPerWatt);
                pointVerticalPosition=graphArea_bottom - (reportedValue - this.verticalScale.minSupportedValue) * this.verticalScale.pixelsPerWatt;
                   
                if (drawingNoDataZone) {
                    if (firstPoint)
                        this.ctx.lineTo(firstPointHorizontalPosition,pointVerticalPosition);
                    this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);
                    this.ctx.stroke();
                    this.ctx.beginPath();
                    drawingNoDataZone=0;
                    firstPoint=1;
                    this.ctx.setLineDash([]); //remove dash
                    this.ctx.lineWidth = 2;
                }
                
                if (firstPoint) {
                    this.ctx.moveTo(pointHorizontalPosition,pointVerticalPosition);
                    firstPoint=0;
                }
                else
                    this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);
            }
                
            lastPointVerticalPosition=pointVerticalPosition;
            lastPointHorizontalPosition=pointHorizontalPosition;

            pointHorizontalPosition -= this.dP.pixels_per_hour ;
            if (pointHorizontalPosition < graphArea_left + 1)
                pointHorizontalPosition=graphArea_left + 1;
            
            var oldDate= new Date(datePointer);
            datePointer.setHours(datePointer.getHours() -1);
            if (datePointer.getTime() == oldDate.getTime() ) {  //summer time change
                datePointer.setHours(datePointer.getHours() - 2);

                pointHorizontalPosition -= this.dP.pixels_per_hour ;
                if (pointHorizontalPosition < graphArea_left + 1)
                    pointHorizontalPosition=graphArea_left + 1;
            }

        }
        //finaly draw the lines
   
        //if the first zone of the chart has no data...
        if (drawingNoDataZone) {
            this.ctx.lineTo(lastPointHorizontalPosition,lastPointVerticalPosition);
            this.ctx.stroke();
        }
        else
            this.ctx.stroke();
        
        
    };
 


 
 
 
 
    
    //=================================================================================================
    //this method draws the chart as a filled chart
    this.drawChartFill = function() {
        //some calculations
        
        //console.log("drawChartFill");
        graphArea_top=this.dP.top_margin;
        graphArea_bottom=this.ctx.canvas.height-this.dP.bottom_margin;
        graphArea_left=this.dP.left_margin;
        graphArea_right=this.ctx.canvas.width-this.dP.right_margin;
        

        //draw from dateEnd to dateStart
        var datePointer= new Date(this.nowDate);
        //console.log("datePointer: " + datePointer);
        
        var firstPoint=1;
        pointHorizontalPosition = graphArea_right - this.dP.number_predicted_hours * this.dP.pixels_per_hour ;
        
        firstPointHorizontalPosition=pointHorizontalPosition;
        lastPointHorizontalPosition=pointHorizontalPosition;
        lastPointVerticalPosition=graphArea_bottom;

        this.ctx.beginPath();
      
        while ( datePointer.getTime() >= this.startDate.getTime() ) {
            isoDate=datePointer.toISOString().slice(0, 19) + 'Z';
            //console.log("testing data for " + isoDate);
            
            if (typeof(this.chartData[isoDate]) !== "undefined") {
                //console.log("Drawing data point for " + isoDate);
                reportedValue=this.chartData[isoDate] * 1.0;
                if (reportedValue) { //if there is data
                    pointVerticalPosition= graphArea_bottom - (reportedValue - this.verticalScale.minSupportedValue) * this.verticalScale.pixelsPerWatt;
                
                    if (firstPoint) {
                        this.ctx.moveTo(firstPointHorizontalPosition,pointVerticalPosition);
                        this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);
                        firstPoint=0;
                    }
                    else
                        this.ctx.lineTo(pointHorizontalPosition,pointVerticalPosition);

                    lastPointVerticalPosition=pointVerticalPosition;
                }

            }

            lastPointHorizontalPosition=pointHorizontalPosition
            pointHorizontalPosition -= this.dP.pixels_per_hour ;
            if (pointHorizontalPosition < graphArea_left + 1)
                pointHorizontalPosition=graphArea_left + 1;

            var oldDate= new Date(datePointer);
            datePointer.setHours(datePointer.getHours() -1);
            if (datePointer.getTime() == oldDate.getTime() ) {  //summer time change
                datePointer.setHours(datePointer.getHours() - 2);

                pointHorizontalPosition -= this.dP.pixels_per_hour ;
                if (pointHorizontalPosition < graphArea_left + 1)
                    pointHorizontalPosition=graphArea_left + 1;
            }
        }

        //close the chart
        this.ctx.lineTo(lastPointHorizontalPosition,lastPointVerticalPosition);
        this.ctx.lineTo(lastPointHorizontalPosition,graphArea_bottom-1);
        this.ctx.lineTo(firstPointHorizontalPosition,graphArea_bottom-1);

        var my_gradient = this.ctx.createLinearGradient(0, 0, 0, graphArea_bottom - graphArea_top);
        my_gradient.addColorStop(0, this.dP.chartColor);
        my_gradient.addColorStop(1, this.dP.chartStartGradientColor);
        this.ctx.fillStyle = my_gradient;

        this.ctx.closePath();
        this.ctx.fill();
        
    };   
    
 
    //=================================================================================================
    //this method loads data from the server and draws the graph
    this.dataArrived = function(data) {
        //console.log("data arrived");
        //console.log(data);
        
        this.chartData=data;
               
        //draw the new data
        this.update();

    };

   
    //=================================================================================================
    //this method loads data from the server and draws the graph
    this.dataPrediction1Arrived = function(data) {
        //console.log("data prediction 1 arrived");
        //console.log(data);
        
        this.chartPrediction1 = data;
               
        //draw the new data
        this.update();
    };


    //=================================================================================================
    //this method loads data from the server and draws the graph
    this.dataPrediction2Arrived = function(data) {
        //console.log("data prediction 2 arrived");
        //console.log(data);
        
        this.chartPrediction2 = data;
               
        //draw the new data
        this.update();
    };
    
    //=================================================================================================
    //this method recalculates the new date to be shown, according with the dragging distance
    this.calculateDragDate = function(horizontalDistance) {
        //console.log(this.nowDate);
        if (horizontalDistance > this.dP.pixels_per_hour || horizontalDistance < -this.dP.pixels_per_hour) {
            numberHours = - Math.trunc( horizontalDistance / this.dP.pixels_per_hour);
            //console.log("numberHours:" + numberHours);
            
            //no not go after the current time
            currentTime=new Date();
            nowDate.setMinutes(0);
            nowDate.setSeconds(0);
            nowDate.setMilliseconds(0);

            if (this.nowDate.getTime() + numberHours * 3600000 > currentTime.getTime())
                numberHours= (currentTime.getTime() - this.nowDate.getTime()) / 3600000;
        
            this.nowDate.setHours(this.nowDate.getHours() + numberHours);
            this.endDate.setHours(this.endDate.getHours() + numberHours);
            this.startDate.setHours(this.startDate.getHours() + numberHours);

            this.update();
        }
        
        return new Date(this.nowDate);
    }

    
};